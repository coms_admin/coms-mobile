<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	</head>
	<body style="margin-left: auto; margin-right: auto; width: 500px;">
		<div id="content">

			<H2 style="width: 500px;">ORM Reverse Engine</H2>
			<p>
				1. Fields with <span style="color:red;">*</span> are required; <br />
				2. All fields should match the database.<br />
				3. It will create two files. <br /> a). /entities/{TableName}.cfc , and  <br />b). /dao/{TableName}DAO.cfc
			</p>
			<table class="table">
				<tr>
					<td>Table Name <span style="color:red;">*</span></td>
					<td style="color: black">
						<input type="text" name="TableName" id="TableName" style="width: 100%">
					</td>
				</tr>
				<tr>
					<td>Primary Key <span style="color:red;">*</span></td>
					<td style="color: black">
						<input type="text" name="tablePK" id="tablePK" style="width: 100%">
					</td>
				</tr>
				
				<tr>
					<td>Parent Key</td>
					<td style="color: black">
						<input type="text" name="parentFK" id="parentFK" style="width: 100%">
					</td>
				</tr>
				
				<tr>
					<td>Overwrite</td>
					<td style="color: black; ">
						<select name="isOverwrite" id="isOverwrite" style="width: 100%">
							<option value="0">-- no --</option>
							<option value="1">-- Entity Only --</option>
                            <option value="2">-- Both --</option>
						</select>
					</td>
				</tr>
				<tr>
					<td></td>
					<td style="text-align: right;">
						<button class="btn btn-success" type="button" id="link_create">Create</button>
					</td>
				</tr>
			</table>
			<div id="div_entity"></div>
			<div id="div_dao"></div>
		</div>
		
		
		
		<!--- ////////////////// --->
		<!--- /// JavaScript /// --->
		<!--- ////////////////// --->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	</body>




	<script>
		var link_create_clicked = function(event){
			event.preventDefault?event.preventDefault():event.returnValue = false; 
			var tableName = $("#TableName").val();
			var tablePK = $("#tablePK").val(); 
			var parentFK = $("#parentFK").val();
			
			var isOverwriteSelect = $("#isOverwrite").val(); 
			var isOverwriteDao  = false;
            var isOverwriteEntity  = false;
			switch (isOverwriteSelect){
				case "0":
                    isOverwriteDao  = false;
                    isOverwriteEntity  = false;
					break;
				case "1":
                    isOverwriteDao  = false;
                    isOverwriteEntity  = true;
					break;
                case "2":
                    isOverwriteDao  = true;
                    isOverwriteEntity  = true;
                    break;
				default :
					break;
			}
			
			$("#div_entity").load("/entities/_ReverseEngine.cfm?tableName="+tableName+"&tablePK="+tablePK+"&parentFK="+parentFK+"&isOverwrite="+isOverwriteEntity);
			$("#div_dao").load("/dao/_ReverseEngine.cfm?tableName="+tableName+"&tablePK="+tablePK+"&parentFK="+parentFK+"&isOverwrite="+isOverwriteDao);
			
		}
		
		$(document).ready(function(){
			$("#link_create").click(link_create_clicked);
		});
	</script>


</html>
