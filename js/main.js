/**
 * Main AngularJS Web Application
 */
var app = angular.module('comsWebApp', [
      'ngRoute',
      'asideModule',
      'ngAnimate'
]);

/**
 * Configure the Routes
 */
app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        // Log In
        .when("/login", {url: "/login", templateUrl: "view/partials/login.cfm", controller: "loginCtrl"})
        // Log In
        .when("/logout", {url: "/logout", templateUrl: "view/partials/login.cfm", controller: "logoutCtrl"})
        // Home
        .when("/", {url: "/login", templateUrl: "view/partials/login.cfm", controller: "loginCtrl"})
        .when("/home", {url: "/home",templateUrl: "view/partials/home.cfm", controller: "defaultCtrl"})
        // search
        .when("/search", {url: "/search",templateUrl: "view/partials/search.cfm", controller: "searchCtrl"})
        // Pages
        .when("/work-order-tasks/:id", {url: "/work-order-tasks/:id", templateUrl: "view/partials/work_order_tasks.cfm", controller: "workOrderTaskCtrl"})
        .when("/work-order-job-completion/:id", {url: "/work-order-job-completion/:id", templateUrl: "view/partials/work_order_job_completion.cfm", controller: "workOrderJobCompletionCtrl"})
        // actions
        .when("/insta/:id", {templateUrl: "view/partials/actions/install.cfm", controller: "installCtrl"})
        .when("/reloc/:id", {templateUrl: "view/partials/actions/relocate.cfm", controller: "relocateCtrl"})
        .when("/remov/:id", {templateUrl: "view/partials/actions/remove.cfm", controller: "removeCtrl"})
        .when("/new/:id", {templateUrl: "view/partials/actions/new.cfm", controller: "newCtrl"})
        .when("/conve/:id", {templateUrl: "view/partials/actions/conversion.cfm", controller: "conversionCtrl"})
        .when("/varia/:id", {templateUrl: "view/partials/actions/variationChange.cfm", controller: "variationChangeCtrl"})
        .when("/confirmation/:id", {templateUrl: "view/partials/confirmation.cfm", controller: "confirmationCtrl"})

        .when("/byJobNumber/:id", {templateUrl: "view/partials/search/byJobNumber.cfm", controller: "byJobNumberCtrl"})
        .when("/byWorkOrderNumber/:id", {templateUrl: "view/partials/search/byWorkOrderNumber.cfm", controller: "byWorkOrderNumberCtrl"})
        .when("/byVenueGMVID/:id", {templateUrl: "view/partials/search/byVenueGMVID.cfm", controller: "byVenueGMVIDCtrl"})

        .otherwise({
          redirectTo:'/home'
        });
}]);


app.controller('workOrderTaskCtrl', function ($scope, $location, $routeParams, $window) {
    console.log("Work Order Task Controller reporting for duty.");
    /*JobID*/
    $scope.id = $routeParams.id;
    /* $scope.pageClass = 'anim-next';*/
    $scope.pageClass = 'page-next';
    $scope.backBtn = 'Back';
    $scope.screenTitle = 'Job';
    $scope.nextBtn = 'Home';
    $scope.showBtn = true;
    $scope.nextBtnClass = 'btn-default';
    //Set Subnav back button destination
    $scope.backCall = function() {
        $scope.pageClass = 'page-back';
        $window.history.back();
    };
    $scope.nextCall = function() {
        $location.path('/home');
    };
    /*load the content for the page using AJAX*/
    $("#workorderContainer").load("view/partials/work_order_tasks_content.cfm?jobID="+ $scope.id);

});

app.controller('workOrderJobCompletionCtrl', function ($scope, $location, $routeParams, $http, $route, $window) {
    console.log("Work Order Job Completion Controller reporting for duty.");
    if(app.authentication() == false){
        /*redirect to login */
        $location.path('/login');
    }
    /*JobID*/
    $scope.id = $routeParams.id;
    $scope.pageClass = 'page-next';
    $scope.backBtn = 'Back';
    $scope.screenTitle = 'Complete';
    $scope.nextBtn = 'Complete';
    $scope.nextBtnClass = 'btn-success';
    $scope.showBtn = true;

    //Set Subnav back button destination
    $scope.backCall = function() {
        $scope.pageClass = 'page-back';
        $window.history.back();
    };

    var data = $.param({
        workOrderJobID: $scope.id,
        statusValue: 5
    });
    var config = { headers : { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}};
    //Set Subnav next button destination
    $scope.dialog = true; //hidden
    $scope.nextCall = function() {
        $http.post('/cfc/WorkOrder.cfc?method=updateWorkOrderJobStatus', data, config)
            .success(function (resdata, status, headers, config) {
                if(resdata["success"] == true || resdata["success"] == 'true'){
                    $scope.dialog = $scope.dialog === false ? true: false; //toggle visibility
                }else{
                    alert(resdata["message"]);
                }
            })
            .error(function (data, status, header, config) {
                console("Error occurs");
            });
    };

    $scope.returnCall = function() {
        $scope.dialog = $scope.dialog === false ? true: false; //toggle visibilty
        $location.path('/home');
    };

    /*load the content for the page using AJAX*/
    $("#workorderContainer").load("view/partials/work_order_job_completion_content.cfm?jobID="+ $scope.id);

});


app.controller('installCtrl', function ($scope, $location, $routeParams, $http, $window) {
    console.log("Installation Controller reporting for duty.");
    $scope.workorderMachineItemID = $routeParams.id.split("_")[0];
    $scope.workOrderID = $routeParams.id.split("_")[1];
    $scope.jobID = $routeParams.id.split("_")[2];

    $scope.pageClass = 'page-next';
    //Subnav Settings
    $scope.backBtn = 'Back';
    $scope.screenTitle = 'Installation';
    $scope.nextBtn = 'Save';
    $scope.showBtn = true;
    $scope.nextBtnClass = 'btn-success';


    //Set Subnav back button destination
    $scope.backCall = function() {
        $scope.pageClass = 'page-back';
        $window.history.back();
    };
    //Set Subnav next button destination
    $scope.nextCall = function() {
        $location.path('/confirmation/'+$routeParams.id);
    };

    $scope.machines = [];

    /*Get Machines */
    var data = $.param({
        machineItemID: $scope.workorderMachineItemID
    });
    var config = { headers : { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}};
    $http.post('/cfc/WorkOrder.cfc?method=Install', data, config)
                      .success(function (resdata, status, headers, config) {
                            if(resdata.success == true){
                                $scope.machines = resdata.machines;
                                if(resdata.IsActionCompleted == 1){
                                    $scope.showBtn = false;
                                }
                            }else{
                                alert(resdata.message);
                            }
                      })
                      .error(function (data, status, header, config) {
                          console("Error occurs");
                      });
});



app.controller('relocateCtrl', function ($scope, $location , $routeParams, $http, $window) {
    console.log("Relocate Controller reporting for duty.");
    $scope.workorderMachineItemID = $routeParams.id.split("_")[0];
    $scope.workOrderID = $routeParams.id.split("_")[1];
    $scope.jobID = $routeParams.id.split("_")[2];

    $scope.pageClass = 'page-next';
    $scope.backBtn = 'Back';
    $scope.screenTitle = 'Relocation';
    $scope.nextBtn = 'Save';
    $scope.nextBtnClass = 'btn-success';
    $scope.showBtn = true;

    //Set Subnav back button destination
    $scope.backCall = function() {
        $scope.pageClass = 'page-back';
        $window.history.back();
    };

    //Set Subnav next button destination
    $scope.nextCall = function() {
        $location.path('/confirmation/'+$routeParams.id);
    };
/*
      /!*Load machine data*!/
      $scope.machines = []
      $scope.machines = [
        {jin: 4, name: 'Dolphin Treasure', serial:123456789, target: 16}
      ]*/
        $scope.machines = [];

        /*Get Machines */
        var data = $.param({
            machineItemID: $scope.workorderMachineItemID
        });
        var config = { headers : { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}};
        $http.post('/cfc/WorkOrder.cfc?method=relocate', data, config)
            .success(function (resdata, status, headers, config) {
                if(resdata.success == true){
                    $scope.machines = resdata.machines;
                    if(resdata.IsActionCompleted == 1){
                        $scope.showBtn = false;
                    }
                }else{
                    alert(resdata.message);
                }
            })
            .error(function (data, status, header, config) {
                console("Error occurs");
            });

});


app.controller('removeCtrl', function ($scope, $location, $routeParams, $http, $window) {
    console.log("Remove Controller reporting for duty.");
    if(app.authentication() == false){
    /*redirect to login */
    $location.path('/login');
    }

    $scope.workorderMachineItemID = $routeParams.id.split("_")[0];
    $scope.workOrderID = $routeParams.id.split("_")[1];
    $scope.jobID = $routeParams.id.split("_")[2];
    $scope.pageClass = 'page-next';

    $scope.backBtn = 'Back';
    $scope.screenTitle = 'Removal';
    $scope.nextBtn = 'Save';
    $scope.nextBtnClass = 'btn-success';
    $scope.showBtn = true;

    //Set Subnav back button destination
    $scope.backCall = function() {
        $scope.pageClass = 'page-back';
        $window.history.back();
    };

    //Set Subnav next button destination
    $scope.nextCall = function() {
    $location.path('/confirmation/'+$routeParams.id);
    };

    $scope.machines = [];

    /*Get Machines */
    var data = $.param({
    machineItemID: $scope.workorderMachineItemID
    });
    var config = { headers : { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}};
    $http.post('/cfc/WorkOrder.cfc?method=remove', data, config)
            .success(function (resdata, status, headers, config) {
                if(resdata.success == true){
                    $scope.machines = resdata.machines;
                    if(resdata.IsActionCompleted == 1){
                        $scope.showBtn = false;
                    }
                }else{
                    alert(resdata.message);
                }
            })
            .error(function (data, status, header, config) {
                console("Error occurs");
            });
});


app.controller('newCtrl', function ($scope, $location, $routeParams, $http , $window) {
    console.log("New Controller reporting for duty.");
    if(app.authentication() == false){
        /*redirect to login */
        $location.path('/login');
    }

    $scope.workorderMachineItemID = $routeParams.id.split("_")[0];
    $scope.workOrderID = $routeParams.id.split("_")[1];
    $scope.jobID = $routeParams.id.split("_")[2];
    $scope.pageClass = 'page-next';
    $scope.backBtn = 'Back';
    $scope.screenTitle = 'New';
    $scope.nextBtn = 'Save';
    $scope.nextBtnClass = 'btn-success';
    $scope.showBtn = true;

    //Set Subnav back button destination
    $scope.backCall = function() {
        $scope.pageClass = 'page-back';
        $window.history.back();
    };

    //Set Subnav next button destination
    $scope.nextCall = function() {
        $location.path('/confirmation/'+$routeParams.id);
    };

    $scope.machines = [];

    /*Get Machines */
    var data = $.param({
        machineItemID: $scope.workorderMachineItemID
    });
    var config = { headers : { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}};
    $http.post('/cfc/WorkOrder.cfc?method=new', data, config)
        .success(function (resdata, status, headers, config) {
            if(resdata.success == true){
                $scope.machines = resdata.machines;
                if(resdata.IsActionCompleted == 1){
                    $scope.showBtn = false;
                }
            }else{
                alert(resdata.message);
            }
        })
        .error(function (data, status, header, config) {
            console("Error occurs");
        });
});


app.controller('conversionCtrl', function ($scope, $location, $routeParams, $http, $window) {
    console.log("conversion Controller reporting for duty.");
    if(app.authentication() == false){
        /*redirect to login */
        $location.path('/login');
    }

    $scope.workorderMachineItemID = $routeParams.id.split("_")[0];
    $scope.workOrderID = $routeParams.id.split("_")[1];
    $scope.jobID = $routeParams.id.split("_")[2];
    $scope.pageClass = 'page-next';
    $scope.backBtn = 'Back';
    $scope.screenTitle = 'Conversion';
    $scope.nextBtn = 'Save';
    $scope.nextBtnClass = 'btn-success';
    $scope.showBtn = true;

    //Set Subnav back button destination
    $scope.backCall = function() {
        $scope.pageClass = 'page-back';
        $window.history.back();
    };

    //Set Subnav next button destination
    $scope.nextCall = function() {
        $location.path('/confirmation/'+$routeParams.id);
    };

    $scope.machines = [];

    /*Get Machines */
    var data = $.param({
        machineItemID: $scope.workorderMachineItemID
    });
    var config = { headers : { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}};
    $http.post('/cfc/WorkOrder.cfc?method=conversion', data, config)
        .success(function (resdata, status, headers, config) {
            if(resdata.success == true){
                $scope.machines = resdata.machines;
                if(resdata.IsActionCompleted == 1){
                    $scope.showBtn = false;
                }
            }else{
                alert(resdata.message);
            }
        })
        .error(function (data, status, header, config) {
            console("Error occurs");
        });
});


app.controller('variationChangeCtrl', function ($scope, $location, $routeParams, $http, $window) {
    console.log("variation Controller reporting for duty.");
    if(app.authentication() == false){
        /*redirect to login */
        $location.path('/login');
    }

    $scope.workorderMachineItemID = $routeParams.id.split("_")[0];
    $scope.workOrderID = $routeParams.id.split("_")[1];
    $scope.jobID = $routeParams.id.split("_")[2];
    $scope.pageClass = 'page-next';
    $scope.backBtn = 'Back';
    $scope.screenTitle = 'Variation Changes';
    $scope.nextBtn = 'Save';
    $scope.nextBtnClass = 'btn-success';

    //Set Subnav back button destination
    $scope.backCall = function() {
        $scope.pageClass = 'page-back';
        $window.history.back();
    };

    //Set Subnav next button destination
    $scope.nextCall = function() {
        $location.path('/confirmation/'+$routeParams.id);
    };

    $scope.machines = [];

    /*Get Machines */
    var data = $.param({
        machineItemID: $scope.workorderMachineItemID
    });
    var config = { headers : { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}};
    $http.post('/cfc/WorkOrder.cfc?method=variationChange', data, config)
        .success(function (resdata, status, headers, config) {
            if(resdata.success == true){
                $scope.machines = resdata.machines;
            }else{
                alert(resdata.message);
            }
        })
        .error(function (data, status, header, config) {
            console("Error occurs");
        });
});

app.authentication = function() {
      if(app.isAuthorized == 1){
        return true;
      }else{
        return false;
      }
};

app.controller('confirmationCtrl', function ($scope, $location, $routeParams, $http, $route, $window) {
    console.log("Confirmation Controller reporting for duty.");
    if(app.authentication() == false){
        /*redirect to login */
        $location.path('/login');
    }

    $scope.workorderMachineItemID = $routeParams.id.split("_")[0];
    $scope.workOrderID = $routeParams.id.split("_")[1];
    $scope.jobID = $routeParams.id.split("_")[2];

    $scope.pageClass = 'page-next';
    $scope.backBtn = 'Back';
    $scope.screenTitle = 'Confirm';
    $scope.nextBtn = 'Confirm';
    $scope.nextBtnClass = 'btn-success';
    $scope.showBtn = true;

    //Set Subnav back button destination
    $scope.backCall = function() {
        $scope.pageClass = 'page-back';
        $window.history.back();
    };

    //Set Subnav next button destination
    $scope.dialog = true; //hidden
    $scope.nextCall = function() {
        $http.post('/cfc/WorkOrder.cfc?method=performAction', data, config)
            .success(function (resdata, status, headers, config) {
                if(resdata["success"] == true || resdata["success"] == 'true'){
                    $scope.dialog = $scope.dialog === false ? true: false; //toggle visibility
                }else{
                    alert(resdata["message"]);
                }
            })
            .error(function (data, status, header, config) {
                console("Error occurs");
            });
    };

    $scope.returnCall = function() {
        $scope.dialog = $scope.dialog === false ? true: false; //toggle visibility
        $location.path('/work-order-tasks/'+$scope.jobID);
    };

    $scope.machines = [];
    /*Get Machines */
    var data = $.param({
        machineItemID: $scope.workorderMachineItemID
        ,workOrderID: $scope.workOrderID
    });
    var config = { headers : { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}};
    $http.post('/cfc/WorkOrder.cfc?method=confirmation', data, config)
        .success(function (resdata, status, headers, config) {
            if(resdata.success == true){
                $scope.machines = resdata.machines;
            }else{
                alert(resdata.message);
            }
        })
        .error(function (data, status, header, config) {
            console("Error occurs");
        });
});

/**
 * Controls all other Pages
 */
app.controller('loginCtrl', function ($scope, $location) {
    console.log("Login Page Controller reporting for duty.");

    $scope.login = function(){
        // submit the login form
        var username = $('#username').val();
        var password = $("#password").val();
        var theData = {
            "username" : username
            ,"password" : password
        }
        $.ajax({
            type: "POST",
            url: "/cfc/User.cfc?method=Login",
            data: theData,
            dataType: "JSON",
            success: function(response){
                if(response.success){
                    /*login*/
                    app.isAuthorized = 1;
                    $location.path('/home');
                    $scope.$apply();
                }else{
                    alert(response.message);
                }
            },
            error: function(response){
                alert('Errors!!');
            }
        });
  }
});

app.controller('logoutCtrl', function ($scope, $location) {
    console.log("logout Page Controller reporting for duty.");
    /*Call the cfc and log out */
    $.ajax({
        type: "POST",
        url: "/cfc/User.cfc?method=Logout",
        data: {},
        dataType: "JSON",
        success: function(response){
            if(response.success){
                /*login*/
                app.isAuthorized = 1;
                $location.path('/login');
                $scope.$apply();
            }else{
                alert(response.message);
            }
        },
        error: function(response){
            alert('Errors!!');
        }
    });
});

app.controller('defaultCtrl', function ( $scope, $location, $http) {
    console.log("Default Page Controller reporting for duty.");
    //var controller = this;
    $scope.pageClass = 'page-next';

    if(app.authentication() == false){
        /*redirect to login */
        $location.path('/login');
    }

    $scope.workOrderJobs = [];
    $scope.isAreaManager = 0;
    $scope.areaManagerJobs = [];

    /*Get Machines */
    var data = $.param({
        isFromWorkOrder: 1
    });
    var config = { headers : { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}};
    $http.post('/cfc/WorkOrder.cfc?method=getAllByUser', data, config)
        .success(function (resdata, status, headers, config) {
            if(resdata.success == true){
                $scope.workOrderJobs = resdata.workOrderJobs;
            }else{
                alert(resdata.message);
            }
        })
        .error(function (data, status, header, config) {
            console.log("Error occurred");
        });

    $http.post('/cfc/Venues.cfc?method=getManagedVenues', data, config)
        .success(function (resdata, status, headers, config) {
            if(resdata.success == true){                
                $scope.venueList = resdata.venueList;
                var venueListString = JSON.stringify(resdata.venueList);
                venueListString = venueListString.replace(/[\[\]]+/g, '');
                if(venueListString.length > 0)
                {
                    $scope.isAreaManager = 1; 
                }
                var am_data = {
                    'venueListString': venueListString,
                    'isFromWorkOrder': 1
                };
                $.ajax({
                    type: "POST",
                    url: '/cfc/WorkOrder.cfc?method=getAllJobsForUserList',
                    contentType: 'application/x-www-form-urlencoded;charset=utf-8;',
                    data: am_data,
                    dataType: 'JSON',
                    success: function(am_resdata){
                        if(am_resdata.success == true){
                            $scope.areaManagerJobs = am_resdata.jobsForUser;
                        }else{
                            alert(am_resdata.message);
                        }
                        $scope.$apply();
                    },
                    error: function(response){
                        console("Error occurred");
                    }
                });
            }
        })
        .error(function (data, status, header, config) {
            console.log("Error occurred");
        });
});

app.controller('searchCtrl', function ($scope, $location) {
    console.log("search Page Controller reporting for duty.");
    if(app.authentication() == false){
        /*redirect to login */
        $location.path('/login');
    }

    $scope.messageSearchByJobNumber = '';
    $scope.messageSearchByWorkOrderNumber = '';
    $scope.messageSearchByVenueGMVID = '';

    $scope.buttonSearchByJobNumberClicked = function() {
        var jobNumber = $("#inputSearchByJobNumber").val();
        
        if(jobNumber.length != 0){
            $scope.messageSearchByJobNumber = "";
            $location.path('/byJobNumber/'+jobNumber);
        }else{
            $scope.messageSearchByJobNumber = 'Please enter a valid Job Number';
        }
    };

    $scope.buttonSearchByWorkOrderNumberClicked = function() {
        var workOrderNumber = $("#inputSearchByWorkOrderNumber").val();
        if(workOrderNumber.length != 0){
            $scope.messageSearchByWorkOrderNumber = "";
            $location.path('/byWorkOrderNumber/'+workOrderNumber);
        }else{
            $scope.messageSearchByWorkOrderNumber = 'Please enter a valid Work Order Number';
        }
    };

    $scope.buttonSearchByVenueGMVIDClicked = function() {
        var venueGMVID = $("#inputSearchByVenueGMVID").val();
        if(venueGMVID.length != 0){
            $scope.messageSearchByVenueGMVID = "";
            $location.path('/byVenueGMVID/'+venueGMVID);
        }else{
            $scope.messageSearchByVenueGMVID = 'Please enter a valid GMVID';
        }
    };
});


app.controller('byJobNumberCtrl', function ($scope, $location, $routeParams, $http, $window) {
    console.log("byJobNumberCtrl Page Controller reporting for duty.");
    if(app.authentication() == false){
        /*redirect to login */
        $location.path('/login');
    }

    $scope.jobNumber = $routeParams.id;
    $scope.backBtn = 'Back';
    $scope.showBtn = true;

    //Set Subnav back button destination
    $scope.backCall = function() {
        $scope.pageClass = 'page-back';
        $window.history.back();
    };

    $scope.workOrderJobs = [];

    /*Get Machines */
    var data = $.param({
        searchValue: $scope.jobNumber,
        searchBy:"Job Number",
        isAreaManager: $scope.isAreaManager
        
    });
    var config = { headers : { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}};
   /*  $http.post('/cfc/Job.cfc?method=getByJobNumber', data, config) */
    $http.post('/cfc/Job.cfc?method=getSuppliersWorkOrderJobs', data, config)
        .success(function (resdata, status, headers, config) {
            if(resdata.success == true){
                $scope.workOrderJobs = resdata.workOrderJobs;
                $scope.keyword = resdata.keyword;
            }else{
                alert(resdata.message);
            }
        })
        .error(function (data, status, header, config) {
            console("Error occurs");
        });

});


app.controller('byWorkOrderNumberCtrl', function ($scope, $location, $routeParams, $http, $window) {
    console.log("byWorkOrderNumberCtrl Page Controller reporting for duty.");
    if(app.authentication() == false){
        /*redirect to login */
        $location.path('/login');
    }
    $scope.workOrderNumber = $routeParams.id;
    $scope.backBtn = 'Back';
    $scope.showBtn = true;

    //Set Subnav back button destination
    $scope.backCall = function() {
        $scope.pageClass = 'page-back';
        $window.history.back();
    };

    $scope.workOrderJobs = [];

    /*Get Machines */
    var data = $.param({
        searchValue: $scope.workOrderNumber,
        searchBy:"Work Number",
        isAreaManager: $scope.isAreaManager
    });
    var config = { headers : { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}};
    $http.post('/cfc/Job.cfc?method=getSuppliersWorkOrderJobs', data, config)
        .success(function (resdata, status, headers, config) {
            if(resdata.success == true){
                $scope.workOrderJobs = resdata.workOrderJobs;
                $scope.keyword = resdata.keyword;
            }else{
                alert(resdata.message);
            }
        })
        .error(function (data, status, header, config) {
            console("Error occurs");
        });

});

app.controller('byVenueGMVIDCtrl', function ($scope, $location, $routeParams, $http, $window) {
    console.log("byVenueGMVIDCtrl Page Controller reporting for duty.");
    if(app.authentication() == false){
        /*redirect to login */
        $location.path('/login');
    }
    $scope.venueGMVID = $routeParams.id;
    $scope.backBtn = 'Back';
    $scope.showBtn = true;

    //Set Subnav back button destination
    $scope.backCall = function() {
        $scope.pageClass = 'page-back';
        $window.history.back();
    };

    $scope.workOrderJobs = [];

    /*Get Machines */
    var data = $.param({
        searchValue: $scope.venueGMVID,
        searchBy:"GMVID",
        isAreaManager: $scope.isAreaManager
    });
    var config = { headers : { 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}};
    $http.post('/cfc/Job.cfc?method=getSuppliersWorkOrderJobs', data, config)
        .success(function (resdata, status, headers, config) {
            if(resdata.success == true){
                $scope.workOrderJobs = resdata.workOrderJobs;
                $scope.keyword = resdata.keyword;
            }else{
                alert(resdata.message);
            }
        })
        .error(function (data, status, header, config) {
            console("Error occurs");
        });
});