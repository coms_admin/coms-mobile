<!-- Include access to menu -->
<div ng-include='"templates/header.html"'></div>
<div class="container-fluid nav-spacing">
    <!---    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-block">
                    <h4 class="card-title">Jobs</h4>
                    <p class="card-text">You are not currently assigned to any jobs.</p>
                </div>
            </div>
        </div>
    </div>--->
    <!-- Work Orders -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4>Work Order Jobs</h4>
                </div>
                <div class="panel-body"><p class="card-text">Choose a Job to carry out a Work Order.</p></div>
                <ul class="list-group" ng-repeat="obj in workOrderJobs">
                    <a href="#/work-order-tasks/{{obj.jobID}}" class="list-group-item">
                        <h4>{{obj.jobName}} <span class="pull-right"><i class="fa fa-lg fa-angle-right"></i></span></h4>
                        <p class="small">
                            Job [{{obj.jobNumber}}]
                            <span class="label {{obj.class}}">
                                {{obj.status}}
                                <strong>
                                    {{obj.completedAction}}/{{obj.totalAction}}
                                </strong>
                            </span>
                        </p>
                    </a>
                </ul>
            </div>           
          
            <div class="panel panel-primary" ng-if="isAreaManager">
                <div class="panel-heading">
                    <h4>Area Manager Jobs</h4>
                </div>
                <div class="panel-body"><p class="card-text">Choose a job to complete.</p></div>
                <ul class="list-group" ng-repeat="obj in areaManagerJobs">
                    <a href="#/work-order-job-completion/{{obj.jobID}}" class="list-group-item">
                        <h4>{{obj.jobName}} <span class="pull-right"><i class="fa fa-lg fa-angle-right"></i></span></h4>
                        <p class="small">
                            Job [{{obj.jobNumber}}]
                            <span class="label {{obj.class}}">
                                {{obj.status}}
                                <strong>
                                    {{obj.completedAction}}/{{obj.totalAction}}
                                </strong>
                            </span>
                        </p>
                    </a>
                </ul>
            </div>
        </div>
    </div>
</div>