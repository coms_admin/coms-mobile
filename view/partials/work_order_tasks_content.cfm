<cfscript>
	oWorkOrder = new cfc.workorder();
	qWorkOrders  = oWorkOrder.getAllByJobID(URL.jobID);

	oJob = new cfc.job();
	qJob =  oJob.getJobDetail(url.jobID);
</cfscript>

<cfoutput>
	<div class="row">
		<div class="col-md-12">
    		<div class="panel panel-primary">
				<div class="panel-heading">
					<h4 class="card-title">#qWorkOrders.venueName# [#qJob.jobNumber#]</h4>
				</div>
				<div class="panel-body">
					<p class="card-text">Choose the detail you would like to complete.</p>
                    <div>
                        <small><b>* Please always perform removal before installation.</b></small> <br/>
						<small><b><span style="color: red">*  NOTE: </span> machine removal must be performed from the QEC dashboard before completing a removal here.</b></small>
                    </div>
				</div>
					<ul class="list-group list-group-flush">
						<cfloop query="qWorkOrders">
							<a href="##/#left(LCase(actionName), 5)#/#workorderMachineItemID#_#workOrderID#_#URL.jobID#" class="list-group-item list-group-item-action">
								#actionName#
								<cfif IsActionCompleted eq 1>
									<span class="label label-info">Completed</span>
								<cfelse>
									<span class="label label-success">New</span>
								</cfif>
								<span class="pull-right"><i class="fa fa-lg fa-angle-right"></i></span>
							</a>
						</cfloop>
					</ul>
				</div>

			</div>
		</div>
	</div>
</cfoutput>
