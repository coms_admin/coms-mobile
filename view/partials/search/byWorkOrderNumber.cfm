<div ng-include='"templates/subnav.html"'></div>
<div class="container-fluid nav-spacing">
    <!-- Work Orders -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4>Search Result</h4>
                    <p>{{keyword}}</p>
                </div>
                <div class="panel-body">
                    <p class="card-text">{{workOrderJobs.length}} record(s) found.</p>
                </div>
                <ul class="list-group" ng-repeat="obj in workOrderJobs">
                    <a href="#/work-order-tasks/{{obj.jobID}}" class="list-group-item">
                        <h4>{{obj.jobName}} <span class="pull-right"><i class="fa fa-lg fa-angle-right"></i></span></h4>
                        <p class="small">
                            Job [{{obj.jobNumber}}]
                            <span class="label {{obj.class}}">
                                {{obj.status}}
                                <strong>
                                    {{obj.completedAction}}/{{obj.totalAction}}
                                </strong>
                            </span>
                        </p>
                    </a>
                </ul>
            </div>
        </div>
    </div>
</div>