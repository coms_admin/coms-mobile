<cfif isdefined("form")>
	<cfdump var="#form#">
</cfif>

<nav class="navbar navbar-fixed-top navbar-dark navbar-inverse">
	<div class="pull-right p-y-1 p-r-1">
		<a class="" href="#/"><img src="img/coms.png"></a>
	</div>
</nav>

<div style="margin-top:15rem;">
	<div class="row">
		<div class="col-sm-10 col-sm-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading"><div class="panel-title">Log In</div></div>
				<div class="panel-body">
					<div class="form-group">
						<label class="text-muted" for="username">Email</label>
						<input type="text" name="username" id="username" value="" placeholder="Your Email Address" class="input-lg form-control" maxlength="255" tabindex="1">
					</div>
					<div class="form-group">
						<label class="text-muted" for="passowrd">Password</label>
						<input type="password" name="password" id="password" value="" placeholder="Your Password" class="input-lg form-control" maxlength="255" tabindex="2">
					</div>
					<div class="row">
						<div class="col-sm-6 col-sm-offset-6">
							<button name="login" tabindex="3" class="btn btn-coms btn-lg btn-block"  ng-click="login()">Log In</button>
						</div>
						<!---<div class="col-sm-6 m-t-3" style="margin: 10px 0;">
							<a href="/coms/assistance/forgotLogon.cfm?u='+username.value" style="color:#333;" class="btn btn-block btn-link">Forgotten Password?</a>
						</div>--->
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 text-center">
			<h2 class="text-muted"><i class="fa fa-question-circle"></i> <a class="text-muted" href="tel:0800266797">0800 266 797</a></h2>
		</div>
	</div>
</div>