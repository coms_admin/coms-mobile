<!-- Include access to menu -->
<div ng-include='"templates/header.html"'></div>
<div class="container-fluid nav-spacing">
    <!-- Work Orders -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4>Search</h4>
                </div>
                <div class="panel-body"><p class="card-text">Job Number</p></div>
                <div style="margin-left:15px;">
                    <input type="text" id="inputSearchByJobNumber"/>
                    <button type="button" name="buttonSearchByJobNumber" id="buttonSearchByJobNumber" ng-click="buttonSearchByJobNumberClicked()" tabindex="3" class="btn btn-info" >search</button>
                    <br/>
                    <p class="alert-danger">{{messageSearchByJobNumber}}</p>
                </div>

                <div class="panel-body"><p class="card-text">Work order Number</p></div>
                <div style="margin-left:15px;">
                    <input type="text" id="inputSearchByWorkOrderNumber"/>
                    <button type="button" name="buttonSearchByWorkOrderNumber" id="buttonSearchByWorkOrderNumber"  ng-click="buttonSearchByWorkOrderNumberClicked()"  tabindex="3" class="btn btn-info" >search</button>
                    <p class="alert-danger">{{messageSearchByWorkOrderNumber}}</p>
                </div>
                <div class="panel-body"><p class="card-text">Venue (GMVID)</p></div>
                <div style="margin-left:15px; margin-bottom: 15px;">
                    <input type="text" id="inputSearchByVenueGMVID"/>
                    <button type="button" name="buttonSearchByVenueGMVID" id="buttonSearchByVenueGMVID"  ng-click="buttonSearchByVenueGMVIDClicked()"  tabindex="3" class="btn btn-info" >search</button>
                    <p class="alert-danger">{{messageSearchByVenueGMVID}}</p>
                </div>
            </div>
        </div>
    </div>
</div>