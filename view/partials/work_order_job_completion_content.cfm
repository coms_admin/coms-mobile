<cfscript>
	oWorkOrder = new cfc.workorder();
	qWorkOrders  = oWorkOrder.getAllByJobID(URL.jobID);

	oJob = new cfc.job();
	qJob =  oJob.getJobDetail(url.jobID);

	allowCompletion = true;
</cfscript>

<cfoutput>
	<div class="row">
		<div class="col-md-12">
    		<div class="panel panel-primary">
				<div class="panel-heading">					
					<h4 class="card-title">
						<cfif qWorkOrders.recordCount gt 0>
							<cfdump var="#qWorkOrders.recordCount#"/>
							#qWorkOrders.venueName# [#qJob.jobNumber#]
						<cfelse>
							Job : #ReplaceNoCase(qJob.jobName, 'Work Order: ', '', 'all')# [#qJob.jobNumber#]
						</cfif>
					</h4>
				</div>
				<div class="panel-body">
					<cfif qWorkOrders.recordCount gt 0>
						<p class="card-text">Complete following Work Order Job by tapping Complete.</p>
						<div>
							<small><b>All Work Order Job tasks should be completed before you mark the Work Order Job complete.</b></small> <br/>
						</div>
					<cfelse>
						<p class="card-text">Complete this Job by tapping Complete.</p>
						<div>
							<small><b>Job Type :#qJob.jobTypeName# </b></small> <br/>
							<small><b>Job Notes / Entry :</b>#qJob.jobNotes# </small> <br/>							
						</div>
					</cfif>
				</div>
				<ul class="list-group list-group-flush">
					<cfloop query="qWorkOrders">
						<span class="list-group-item list-group-item-action">
							#actionName#
							<cfif IsActionCompleted eq 1>
								<span class="label label-info">Completed</span>
							<cfelse>
								<cfif allowCompletion>
									<cfset allowCompletion = false>
								</cfif>
								<span class="label label-success">New</span>
							</cfif>
						</span>
					</cfloop>
				</ul>
			</div>
		</div>
	</div>
</cfoutput>
