<div ng-include='"templates/subnav.html"'></div>
<div class="container-fluid nav-spacing">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary" ng-data="obj in machines">
				<div class="panel-heading">
					<h4>Remove the listed machines.</h4>
				</div>
				<div class="panel-body">
					<p class="card-text"><em>Carry out the work then tap Save.</em></p>
				</div>
				<table class="table" ng-repeat="obj in machines">
					<thead>
						<tr>
							<th style="width:20%">JIN</th>
							<th>Machine</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="obj in machines">
							<td class="bg-warning">{{obj.jin}}</td>
							<td>{{obj.name}}<br>
                                <small>{{obj.serial}}
									<span class="label {{obj.class}}">
                                        {{obj.status}}
                                    </span>
                                </small>
							</td>
							<td style="vertical-align:middle;"><i class="fa fa-lg fa-times"></i></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>