<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>

<!-- Meta-Information -->
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="COMS">
    <title>COMS | App Name</title>
    <link rel="shortcut icon" href="img/favicon.ico">
    <!-- Apple/Android Startup & Icons -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="apple-touch-startup-image" href="/startup.png">
    <link href="img/apple-touch-icon.png" rel="apple-touch-icon" />
    <link href="img/apple-touch-icon-76x76.png" rel="apple-touch-icon" sizes="76x76" />
    <link href="img/apple-touch-icon-120x120.png" rel="apple-touch-icon" sizes="120x120" />
    <link href="img/apple-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152" />
    <!-- Windows Tiles -->
    <meta name="application-name" content="COMS Web App"/>
	<meta name="msapplication-square70x70logo" content="small.jpg"/>
	<meta name="msapplication-square150x150logo" content="medium.jpg"/>
	<meta name="msapplication-wide310x150logo" content="wide.jpg"/>
	<meta name="msapplication-square310x310logo" content="large.jpg"/>
	<meta name="msapplication-TileColor" content="#25282b"/>

    <script src="https://use.fontawesome.com/b345b3f71e.js"></script>

    <!-- Our Website CSS Styles -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/aside-menu.css">

</head>
<body ng-app="comsWebApp">
    <input name="isAuthorized" ng-value="isAuthorized" ng-init="1" class="hidden">

<!--[if lt IE 7]>
<!-- etc… 
<![endif]-->
    <!-- Our Menu -->
    <aside-menu id="menu-1" side="left" width="70%" is-backdrop="true" push-content="true">
        <div ng-include='"templates/menu.cfm"'></div>
    </aside-menu>
    
    <!--- <div ng-include='"templates/notify.html"'></div> --->
    <!-- Our Website Content Goes Here -->
    <aside-menu-content>
        <div class="page {{ pageClass }}" ng-view></div>
    </aside-menu-content>

<!-- Vendor: Javascripts -->
<!-- etc… -->

<!-- Vendor: Angular, followed by our custom Javascripts -->

<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.29/angular-route.min.js"></script>
<script src="js/aside-menu.min.js"></script>
<script src="js/angular-animate.js"></script>
<!-- <script src="js/ngDraggable.js"></script> -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js" integrity="sha384-THPy051/pYDQGanwU6poAc/hOdQxjnOEXzbT+OuUAFqNqFjL+4IGLBgCJC3ZOShY" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.2.0/js/tether.min.js" integrity="sha384-Plbmg8JY28KFelvJVai01l8WyZzrYWG825m+cZ0eDDS1f7d/js6ikvy1+X+guPIB" crossorigin="anonymous"></script>

<!-- Bootstrap 3 -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!-- Our Website Javascripts -->
<script src="js/main.js"></script>

<cfif isdefined("session.userID")>
    <script>
        app.isAuthorized = 1;
    </script>
<cfelse>
    <script>
        app.isAuthorized = 0;
    </script>
</cfif>
</body>
</html>