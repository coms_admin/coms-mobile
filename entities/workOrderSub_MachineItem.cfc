<cfcomponent  extends="entities._AbstractEntity">


	<!--- This class is created by [ReverseEngine] on {ts '2016-08-12 12:27:32'}----> 

	<cfproperty name="ID" type="string" />
	<cfproperty name="MachineFK" type="string" />
	<cfproperty name="JIN" type="numeric" />
	<cfproperty name="SerialNumber" type="string" />
	<cfproperty name="ExistingGameName" type="string" />
	<cfproperty name="WarehouseFK" type="string" />
	<cfproperty name="ManufacturerFK" type="string" />
	<cfproperty name="GANFK" type="string" />
	<cfproperty name="ProposedGameFK" type="string" />
	<cfproperty name="ActionFK" type="string" />
	<cfproperty name="Cost" type="numeric" />
	<cfproperty name="RecordCreatedon" type="date" />
	<cfproperty name="RecordCreatedBy" type="string" />
	<cfproperty name="woSub_PO_number" type="string" />

	<!---////////////// table name and primary key /////////////////---> 


	<cffunction name="init" returntype="workOrderSub_MachineItem">
		<cfset this.tableName = "workOrderSub_MachineItem">
		<cfset this.tablePK = "ID">
		<cfset this.parentFK = "MachineFK">
		<cfreturn this />
	</cffunction>

</cfcomponent>
