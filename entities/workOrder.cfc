<cfcomponent  extends="entities._AbstractEntity">


	<!--- This class is created by [ReverseEngine] on {ts '2016-08-12 12:26:04'}----> 

	<cfproperty name="ID" type="string" />
	<cfproperty name="accnoFK" type="numeric" />
	<cfproperty name="contactFK" type="string" />
	<cfproperty name="contractStartDate" type="date" />
	<cfproperty name="contractExpiryDate" type="date" />
	<cfproperty name="percentageLimitD" type="numeric" />
	<cfproperty name="percentageAuthorised" type="numeric" />
	<cfproperty name="personRespossibleFK" type="string" />
	<cfproperty name="CreatedOn" type="date" />
	<cfproperty name="CreatedByFK" type="string" />
	<cfproperty name="LastUpdatedOn" type="date" />
	<cfproperty name="LastUpdatedByFK" type="string" />
	<cfproperty name="Status" type="numeric" />
	<cfproperty name="Number" type="numeric" />
	<cfproperty name="proposedDateOfInstallation" type="date" />
	<cfproperty name="ApprovedByFK" type="string" />
	<cfproperty name="ApprovedOn" type="date" />
	<cfproperty name="Deleted" type="boolean" />
	<cfproperty name="LabourCosts" type="numeric" />
	<cfproperty name="ShippingCosts" type="numeric" />
	<cfproperty name="TotalCosts" type="numeric" />
	<cfproperty name="CheckedByFK" type="string" />
	<cfproperty name="CheckedOn" type="date" />
	<cfproperty name="CommonInstallerFK" type="numeric" />

	<!---////////////// table name and primary key /////////////////---> 


	<cffunction name="init" returntype="workOrder">
		<cfset this.tableName = "workOrder">
		<cfset this.tablePK = "ID">
		<cfset this.parentFK = "accnoFK">
		<cfreturn this />
	</cffunction>

</cfcomponent>
