<cfcomponent  extends="entities._AbstractEntity">


	<!--- This class is created by [ReverseEngine] on {ts '2016-08-12 12:28:43'}----> 

	<cfproperty name="ID" type="string" />
	<cfproperty name="actionOrder" type="numeric" />
	<cfproperty name="actionName" type="string" />

	<!---////////////// table name and primary key /////////////////---> 


	<cffunction name="init" returntype="workOrder_lib_machine_Action">
		<cfset this.tableName = "workOrder_lib_machine_Action">
		<cfset this.tablePK = "ID">
		<cfreturn this />
	</cffunction>

</cfcomponent>
