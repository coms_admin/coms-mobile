<!--- ///////////////////////////////////////////////// --->
<!--- ///////////////////////////////////////////
/////////////////////////////////////////////////
/////////////////////////////////////////////////

This page will generate an entity.

1. TableName: should be the database table name.
2. TableFK: the table primary key
3. ParentFK: optional
4. isOverwrite: if true, it will delete the existing entity and create a new one.

/////////////////////////////////////////// --->

<cfset tableName = URL.tableName /> <!--- required --->
<cfset tablePK = URL.tablePK /> <!--- required --->
<cfset parentFK = URL.parentFK /> <!--- optional --->
<cfset isOverwrite = URL.isOverwrite /> <!--- optional --->

<h2>Entity</h2>

<cfif len(tableName) lte 1 OR len(tablePK)  lte  1>
	<h2>TableName and TablePF are required !!</h2>
	<cfabort />
</cfif>

<!---- Read Table ---->
<cfset qData = "" />
<cftry>
	<cfquery name="qData" datasource="#session.datasource#">
		SELECT top 1 * FROM [#tableName#] 
	</cfquery>
	<cfcatch>
		<cfoutput>
			<p>
				Unable to load Database table 
				<strong>[#session.datasource#].[#tableName#]</strong>
				!!
			</p>
		</cfoutput>
		<cfabort />
	</cfcatch>
</cftry>
<cfset tableMeta = GetMetaData(qData) />
<cfset filePath = expandPath( "./#tableName#.cfc" ) />
<!---
	Delete the file if it exists so that we don't keep populating
	the same document.
	--->
<cfif fileExists( filePath )>
	<cfif isOverwrite>
		<cfset fileDelete( filePath ) />
	<cfelse>
		<cfoutput>
			<div style="text-align: left; font-size: 20px;">
				<p>
					1. <strong>#tableName#.cfc</strong> has already existed. 
				</p>
				<p>
					2. Please set <<strong style="color: red;">Overwrite = true</strong>> if you want overwrite the existing entity.
				</p>
			</div>
		</cfoutput>
		<cfabort>
	</cfif>
	
</cfif>
<!---
	Create a file file object that we can write to. We have to
	explicitly define the Mode as Append otherwise the file will
	be opened on read mode.
	--->
<cfset dataFile = fileOpen( filePath, "append" ) />
<!--- Header of the class --->
<cfset fileWriteLine(dataFile,'<cfcomponent  extends="entities._AbstractEntity">
') />
<cfoutput>
	<cfset fileWriteLine(dataFile,'') />
	<cfset fileWriteLine(dataFile,'	<!--- This class is created by [ReverseEngine] on #now()#----> ') />
	<cfset fileWriteLine(dataFile,'') />
</cfoutput>
<!--- table property --->
<cfloop array="#tableMeta#" index="columnStruct">
	<cfset colType = getCFType(columnStruct.TypeName) />
	<cfif len(colType) gt 0>
		<cfset fileWriteLine(dataFile,'	<cfproperty name="#columnStruct.Name#" type="#colType#" />') />
	<cfelse>
		<cfset fileWriteLine(dataFile,'	<cfproperty name="#columnStruct.Name#"/>') />
	</cfif>
</cfloop>
<cfset fileWriteLine(dataFile,'') />
<cfset fileWriteLine(dataFile,'	<!---////////////// table name and primary key /////////////////---> ') />
<cfset fileWriteLine(dataFile,'') />
<!--- Init part --->
<cfset fileWriteLine(dataFile,'
	<cffunction name="init" returntype="#tableName#">') />
<cfset fileWriteLine(dataFile,'		<cfset this.tableName = "#tableName#">') />
<cfset fileWriteLine(dataFile,'		<cfset this.tablePK = "#tablePK#">') />
<cfif isDefined("parentFK") and  len(parentFK) gt 0 >
	<cfset fileWriteLine(dataFile,'		<cfset this.parentFK = "#parentFK#">') />
</cfif>
<cfset fileWriteLine(dataFile,'		<cfreturn this />') />
<cfset fileWriteLine(dataFile,'	</cffunction>') />
<cfset fileWriteLine(dataFile,'') />
<cfset fileWriteLine(dataFile,'</cfcomponent>') />
<!---
	Now that we have finished writing the file, let's close it.
	This will close the stream which should prevent any unintented
	locking on the file access.
	--->
<cfset fileClose( dataFile ) />
<cfoutput><strong>
		#tableName#.cfc
	</strong>
	has been created.</cfoutput>

<cffunction name="getCFType" access="private" returntype="string">
	<cfargument name="typeName" required="true" />
	<cfset cfType = "" />
	<cfswitch expression="#arguments.typeName#">
		<!--- bigint --->
		<cfcase value="bigint"><cfset cfType = "numeric" /></cfcase>
		<!--- binary --->
		<cfcase value="binary"><cfset cfType = "string" /></cfcase>
		<!--- bit --->
		<cfcase value="bit"><cfset cfType = "boolean" /></cfcase>
		<!--- char --->
		<cfcase value="char"><cfset cfType = "string" /></cfcase>
		<!--- date --->
		<cfcase value="date"><cfset cfType = "date" /></cfcase>
		<!--- datetime --->
		<cfcase value="datetime"><cfset cfType = "date" /></cfcase>
		<!--- datetime2 --->
		<cfcase value="datetime2"><cfset cfType = "date" /></cfcase>
		<!--- datetimeoffset --->
		<cfcase value="datetimeoffset"><cfset cfType = "date" /></cfcase>
		<!--- decimal --->
		<cfcase value="decimal"><cfset cfType = "numeric" /></cfcase>
		<!--- float --->
		<cfcase value="float"><cfset cfType = "numeric" /></cfcase>
		<!--- geography --->
		<cfcase value="geography"><cfset cfType = "string" /></cfcase>
		<!--- geometry --->
		<cfcase value="geometry"><cfset cfType = "string" /></cfcase>
		<!--- hierarchyid --->
		<cfcase value="hierarchyid"><cfset cfType = "string" /></cfcase>
		<!--- image --->
		<cfcase value="image"><cfset cfType = "binary" /></cfcase>
		<!--- int --->
		<cfcase value="int"><cfset cfType = "numeric" /></cfcase>
		<!--- money --->
		<cfcase value="money"><cfset cfType = "numeric" /></cfcase>
		<!--- nchar --->
		<cfcase value="nchar"><cfset cfType = "string" /></cfcase>
		<!--- ntext --->
		<cfcase value="ntext"><cfset cfType = "string" /></cfcase>
		<!--- numeric --->
		<cfcase value="numeric"><cfset cfType = "numeric" /></cfcase>
		<!--- nvarchar --->
		<cfcase value="nvarchar"><cfset cfType = "string" /></cfcase>
		<!--- nvarchar(max) --->
		<cfcase value="nvarchar(max)"><cfset cfType = "string" /></cfcase>
		<!--- real --->
		<cfcase value="real"><cfset cfType = "numeric" /></cfcase>
		<!--- smalldatetime --->
		<cfcase value="smalldatetime"><cfset cfType = "date" /></cfcase>
		<!--- smallint --->
		<cfcase value="smallint"><cfset cfType = "numeric" /></cfcase>
		<!--- smallmoney --->
		<cfcase value="smallmoney"><cfset cfType = "numeric" /></cfcase>
		<!--- sql_variant --->
		<cfcase value="sql_variant"><cfset cfType = "string" /></cfcase>
		<!--- text --->
		<cfcase value="text"><cfset cfType = "string" /></cfcase>
		<!--- time --->
		<cfcase value="time"><cfset cfType = "date" /></cfcase>
		<!--- timestamp --->
		<cfcase value="timestamp"><cfset cfType = "date" /></cfcase>
		<!--- tinyint --->
		<cfcase value="tinyint"><cfset cfType = "numeric" /></cfcase>
		<!--- uniqueidentifier --->
		<cfcase value="uniqueidentifier"><cfset cfType = "string" /></cfcase>
		<!--- varbinary --->
		<cfcase value="varbinary"><cfset cfType = "binary" /></cfcase>
		<!--- varbinary(max) --->
		<cfcase value="varbinary(max)"><cfset cfType = "binary" /></cfcase>
		<!--- varchar --->
		<cfcase value="varchar"><cfset cfType = "string" /></cfcase>
		<cfcase value="varchar(max)"><cfset cfType = "string" /></cfcase>
		<!--- xml --->
		<cfcase value="xml"><cfset cfType = "string" /></cfcase>
		<cfdefaultcase><cfset cfType = "" /></cfdefaultcase>
	</cfswitch>
	<cfreturn cfType />
</cffunction>
