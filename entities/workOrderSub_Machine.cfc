<cfcomponent  extends="entities._AbstractEntity">


	<!--- This class is created by [ReverseEngine] on {ts '2016-08-12 12:27:11'}----> 

	<cfproperty name="ID" type="string" />
	<cfproperty name="WorkOrderFK" type="string" />
	<cfproperty name="FreightCost" type="numeric" />
	<cfproperty name="InstallationCost" type="numeric" />
	<cfproperty name="LicenceFee" type="numeric" />
	<cfproperty name="TotalCost" type="numeric" />
	<cfproperty name="Notes" type="string" />
	<cfproperty name="woSub_PO_number" type="string" />
	<cfproperty name="WarehouseConversion" type="string" />

	<!---////////////// table name and primary key /////////////////---> 


	<cffunction name="init" returntype="workOrderSub_Machine">
		<cfset this.tableName = "workOrderSub_Machine">
		<cfset this.tablePK = "ID">
		<cfset this.parentFK = "WorkOrderFK">
		<cfreturn this />
	</cffunction>

</cfcomponent>
