<!---
	Name        : $COMPONENT_PATH$.Application
	Author      : Bruce Trevarthen 
	Created     : 17th July 2013
	Version     : 1.0
	Last Updated: 
	History     : 
	Purpose     : Initiate the application
	Notes       : The main point to remember here is that onApplicationEnd() and onSessionEnd() happen asynchronously 
	              - so there is no request and therefore no associated browser.
	              
	              The other thing to remember is to NOT include the functions onRequest() and onRequestEnd() 
	              in the $COMPONENT_PATH$.Application if the cfcs in the $COMPONENT_PATH$ needs to be externally accessable
	              - FlashRemoting, Webservice, etc.
--->
<cfcomponent output="false"><cfsilent>
	<!--- Define application parameters --->
	<cfscript>
		this.name = "COMS_MOBILE_#replace(listGetAt(cgi.SERVER_NAME,1,'.'),'-','_')#";
		// How long application vars persist 
		this.applicationTimeout = createTimeSpan(0,24,0,0);
		// Should client vars be enabled? 
		this.clientManagement = true;
		// Where should we store them, if enable? 
		this.clientStorage = "registry";
		// Where should cflogin stuff persist 
		this.loginStorage = "session";
		// Should we even use sessions? 
		this.sessionManagement = true;
		// How long do session vars persist? 
		this.sessionTimeout = createTimeSpan(0,8,0,0);
		// Should we set cookies on the browser? 
		this.setClientCookies = true;
		// should cookies be domain specific, ie, *.foo.com or www.foo.com 
		this.setDomainCookies = false;
		// should we try to block 'bad' input from users 
		this.scriptProtect = false;
		// should we secure our JSON calls? 
		this.secureJSON = false;
		// Should we use a prefix in front of JSON strings? 
		this.secureJSONPrefix = "";
		// Used to help CF work with missing files and dir indexes 
		// this.welcomeFileList = "";
		
		// define custom coldfusion mappings. Keys are mapping names, values are full paths
		this.mappings = structNew();
		// define a list of custom tag paths.
		this.customtagpaths = "";
		// Define where to get the settings
		this.settingsFile = ExpandPath("/") & "_settings\settings.ini";		
	</cfscript>

	<!---
		Name        : onApplicationStart
		Access      : public
		Author      : Bruce Trevarthen 
		Created     : 17th July 2013
		Version     : 1.0
		Last Updated: 
		History     : 
		Purpose     : Define global information and settings for the current Application
		Notes       : The application first starts: the first request for a page is processed or 
		              the first CFC method is invoked by an event gateway instance, or a web services or Macromedia Flash Remoting CFC
		Arguments   : N/A
		Return      : boolean
	--->
	<cffunction name="onApplicationStart" returnType="boolean" output="false">
		<!--- Application is starting up, load the Config file --->
 		<cfset var cfgFile	= this.settingsFile />
		<cfset var configName = cgi.server_name />
		<cfif not listFindNoCase(structKeyList(getProfileSections(cfgFile)),configName)>
			<cfset configName = 'Default' />
		</cfif>
		
		<!--- CFC Instances --->
		<cflock scope="APPLICATION" type="EXCLUSIVE" timeout="10">
			<cfscript>
				application.applicationName=this.name;
				// Application Root URL
				application.rootURL = "http://#cgi.SERVER_NAME#:#cgi.SERVER_PORT#/";
				//
				// DSN for this system 
				application.datasource = configParam(cfgFile,configName,"datasource");
				application.datasource_poms = configParam(cfgFile,configName,"datasource_poms");
				application.datasource_gmpa = configParam(cfgFile,configName,"datasource_gmpa");
				application.datasource_multiSync = configParam(cfgFile,configName,"datasource_multiSync");
				// Administrator Email Address for this system 
	            application.administratorEmail = configParam(cfgFile, configName, "administratorEmail");
				// This is the link to the system, it should contain the full path.
				application.rootURL = "http://#cgi.SERVER_NAME#:#cgi.SERVER_PORT#/";
				// This is the absolut file path to the system, it should contain the full path. 
				application.rootPath = expandPath("\");
				// This is the key used to encrypt passwords etc
				application.encryptionKey = configParam(cfgFile, configName, "encryptionKey");
				// This is the dot notated path to the components directory 	
				application.componentPath = configParam(cfgFile, configName, "clientComponentPath");
				//
			</cfscript>
		</cflock>
		
		<!--- Make a note that the app started up --->
		<cflog file="#this.name#" type="Information" text="Application Started" />
		<!--- Application started successfully - Return true --->
		<cfreturn true />
		
	</cffunction>
	
	
	<!---
		Name        : onApplicationEnd
		Access      : public
		Author      : Bruce Trevarthen 
		Created     : 17th July 2013
		Version     : 1.0
		Last Updated: 
		History     : 
		Purpose     : Notify of Clean up stuff before the application shuts down
		Notes       : The application ends: the application times out, or the server is stopped
		Arguments   : N/A
		Return      : boolean
	--->
	<cffunction name="onApplicationEnd" returnType="void" output="false">
		<cfargument name="ApplicationScope" required=true />
	</cffunction>
	
	
	<!---
		Name        : onRequestStart
		Access      : public
		Author      : Bruce Trevarthen 
		Created     : 17th July 2013
		Version     : 1.0
		Last Updated: 
		History     : 
		Purpose     : - define global variables/settings before loading the page 
		              - secure the site with forcing the user to login
		              - only allowing index.cfm to be requested
		              - etc., etc. 
		Notes       : A request starts
		Arguments   : String: targetPage - The requested page
		Return      : boolean
	--->
	<cffunction name="onRequestStart" returnType="boolean"  output="true">
		<cfargument name="targetPage" type="String" required=true/>

		<cfscript>
			processSearchEngineSafeURL();
		</cfscript>		

		<cfif StructKeyExists(url, "reinit")>
			<cfscript>
				writeoutput("reinitialising");
				structClear(application);
				structClear(session);
				onApplicationStart("/");
			</cfscript>
			<cflocation url="/" addtoken="no" />
		</cfif>
        	
		<cfreturn true />
	</cffunction>
	
	
	<!---
		Name        : onSessionStart
		Access      : public
		Author      : Bruce Trevarthen 
		Created     : 17th July 2013
		Version     : 1.0
		Last Updated: 
		History     : 
		Purpose     : - define global variables/settings for the session
		              - track new sessions
		              - etc., etc.
		Notes       : A session starts
		Arguments   : N/A
		Return      : void
	--->
	<cffunction name="onSessionStart" returnType="void" output="false">
		<!--- <cfset application.sessionCount = application.sessionCount + 1> --->
		<!--- session CFC instance stack retrieval will go here --->
	</cffunction>
	
	<!---
		Name        : onSessionEnd
		Access      : public
		Author      : Bruce Trevarthen 
		Created     : 17th July 2013
		Version     : 1.0
		Last Updated: 
		History     : 
		Purpose     : - clean up closed sessions
		              - track closed sessions
		              - etc., etc.
		Notes       : A session ends
		Arguments   : N/A
		Return      : void
	--->
	<cffunction name="onSessionEnd" returnType="void" output="false">
		<!--- <cfset application.sessionCount = application.sessionCount - 1> --->
		<!--- session CFC instance stack insertion/return will go here --->
	</cffunction>


<!--- MISC Functions --->

	<!---
		Name        : configParam
		Access      : public
		Author      : Bruce Trevarthen
		Created     : 17th July 2013
		Version     : 1.01
		Last Updated: 17th July 2013
		History     : 
		Purpose     : Reader of the config/ini-file
		Notes       : Try to get name.key in ini file, and if not, default to default.key. 
		              Also support %default% expansion, which just means replace with default value
		Arguments   : String: iniFile - The absolute path of the ini-file
		              String: name - The label of the ini-file Section
		              String: key - The name of the ini-file Key
		Return      : returns the String value of the requested name.key
	--->
	<cffunction name="configParam" output="false" returnType="string" access="public" hint="Try to get name.key in ini file, and if not, default to default.key. Also support %default% expansion, which just means replace with default value">
		<cfargument name="iniFile" type="string" required="true" />
		<cfargument name="name" type="string" required="true" />
		<cfargument name="key" type="string" required="true" />

		<!--- Look for the "name" supplied --->
		<cfset var result = getProfileString(inifile,name,key) />
		
		<!--- Didn't find it then use default --->
		<cfif result eq "">
			<cfset result = getProfileString(inifile,"Default",key) />
		</cfif>
		
		<cfreturn result />
	</cffunction>	

	<!---
		Name        : processSearchEngineSafeURL
		Access      : public
		Author      : Bruce Trevarthen 
		Created     : 17th July 2013
		Version     : 1.0
		Last Updated: 
		History     : 
		Purpose     : Process Search Engine Safe URL's and injects them into the URL scope
		Notes       : 
		Arguments   : N/A
		Return      : void
	--->
	<cffunction name="processSearchEngineSafeURL" output="false" returnType="void" access="public" hint="Process Search Engine Safe URL's and injects them into the URL scope">
		<cfscript>
			// Get the potential Parameter List
			var urlStr = cgi.PATH_INFO;
			// Split the String to separate possible "Pass through Parameters"
			var urlParam = urlStr.split('"');
			var urlParamArr = "";
			var tmpStr = "";
			var javaArray = "";
			var javaVector = "";
			var i = "";
	
			// If the urlStr > 0 AND it doesn't contain the SCRIPT_NAME
			if( Len(urlStr) gt 0 and Find(urlStr,cgi.SCRIPT_NAME) eq 0) {
				// Initiate Java Arrays and Vector Objects
				javaArray = CreateObject("java", "java.util.Arrays");
				javaVector = CreateObject("java", "java.util.Vector");
				// Initiate the URL parameter Array
				urlParamArr = javaVector.init();
	
				// Loop over the possible "Pass through Parameters"
				for( i = 1; i lte ArrayLen(urlParam); i = i + 1 ) {
					tmpStr = urlParam[i]; 
					// Remove the leading "/"
					if( Mid(tmpStr, 1, 1) eq "/" ) {
						tmpStr = Mid(tmpStr, 2, Len(tmpStr));
					}
					
					// Either add the "Pass through Parameters" as is to the URL Parameter Array
					// Or Split up the SES URK parameters and add them to the URL Parameter Array
					switch( i mod 2 ) {
						case(0): urlParamArr.add(tmpStr); break;
						default:
							urlParamArr.addAll(javaVector.init(javaArray.asList(tmpStr.split("/"))));
					}
				}
				
				// Add the URL Parameters from the URL Parameter Array to the URL scope
				for( i = 1; i lte ArrayLen(urlParamArr); i = i + 2 ) {
					url[urlParamArr[i]] = "";
					if( i lt ArrayLen(urlParamArr) ) {
						url[urlParamArr[i]] = urlParamArr[i+1];
					}
				}
			}
		</cfscript>
	</cffunction>	

	<!---
		Name        : onError
		Access      : public
		Author      : Bruce Trevarthen 
		Created     : 17th July 2013
		Version     : 1.0
		Last Updated: 
		History     : 
		Purpose     : Error/Exception Handling
		Notes       : 
		Arguments   : N/A
		Return      : void
	--->
	<cffunction name="onError">
	
		<cfargument
			name="Exception"
			required="true" />
			
		<cfargument
			type="String"
			name="EventName"
			required="true" />
		<!--- Log all errors. --->
		<cflog file="#This.Name#"
			type="error"
			text="Event Name: #Arguments.Eventname#">
			
		<cflog file="#This.Name#"
			type="error"
			text="Message: #Arguments.Exception.message#">
			
		<cflog file="#This.Name#"
			type="error" 
			text="Root Cause Message: #Arguments.Exception.rootcause.message#"> 
		
		<!--- Display an error message if there is a page context. --->
		<cfif isDefined("arguments.exception.rootCause")>
		<cfif arguments.exception.rootCause eq "coldfusion.runtime.AbortException">
			<cfreturn/>
		</cfif>
		</cfif>
		<cfif NOT ((Arguments.EventName is "onSessionEnd") OR (Arguments.EventName is "onApplicationEnd"))>
			<cfdump var="#arguments#">
			<!--- <cfmail to="bruce@layerx.co.nz" from="debug@layerx.co.nz" subject="COMS ADMIN Error" type="html"><cfdump var="#Arguments#" /></cfmail> --->
			<cfabort />
		</cfif>
	</cffunction>

</cfsilent></cfcomponent>