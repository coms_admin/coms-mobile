<cfcomponent>
	<cfset obj = ''>
	
	<cffunction name="getOneObject" access="public" output="false" returntype="any" hint="Return The Object">
		<cfargument name="tableID" type="any" required="true">
		
		<cfset var qGet = ''>
		<cfquery name="qGet" datasource="#session.datasource#">
			SELECT * FROM [#obj.tableName#]
			WHERE 1 = 1
			<cfif isDefined("arguments.tableID") and len(arguments.tableID) gt 0>
				AND #obj.tablePK# = <cfqueryparam value="#arguments.tableID#"> ;
			</cfif>
		</cfquery>

		<!--- Create an object --->
		<cfset var newObj = Duplicate(obj)>
		
		<cfset var columns = qGet.ColumnList>
		<cfloop list="#columns#" index="col" >
			<cfset attrValue = ''>
			<cftry>
				<cfset attrValue = qGet['#col#']>
				<cfcatch></cfcatch>
			</cftry>
			<cfif len(attrValue) gt 0>
					<cfset newObj["#col#"] = attrValue>
			<cfelse>
					<cfset newObj["#col#"] = "">
			</cfif>
		</cfloop>
		<cfreturn newObj>
		
	</cffunction>

	<cffunction name="getOneQuery" access="public" output="false" returntype="query" hint="Return The Query">
		<cfargument name="tableID" type="any" required="true">
		
		<cfset var qGet = ''>
		<cfquery name="qGet" datasource="#session.datasource#">
			SELECT * FROM [#obj.tableName#]
			WHERE 1 = 1
			<cfif isDefined("arguments.tableID") and len(arguments.tableID) gt 0>
				AND #obj.tablePK# = <cfqueryparam value="#arguments.tableID#">
			</cfif>
			;	
		</cfquery>
		
		<cfreturn qGet>
	</cffunction>
	
	<cffunction name="getAllQuery" access="public" output="false" returntype="query" hint="return matched query">
		<cfargument name="orderBy" type="any" required="false">
		<cfargument name="isASC" type="any" required="false" default="true">
		<cfargument name="amount" type="any" required="false" default="10000">
		
		<cfset var qGet = ''>
		<cfquery name="qGet" datasource="#session.datasource#">
			SELECT TOP #arguments.amount# * FROM [#obj.tableName#]
			<cfif isDefined("arguments.orderBy") and len(arguments.orderBy) gt 1>
				Order BY [#arguments.orderBy#]
				<cfif arguments.isASC>
					ASC
				<cfelse>
					DESC
				</cfif>
			</cfif>
			;
		</cfquery>
		<cfreturn qGet>
	</cffunction>
	
	<cffunction name="getListByParentFK" access="public" output="false" returntype="query" hint="">
		
		<cfargument name="parentFK" type="any" required="true">
		<cfargument name="orderBy" type="any" required="false">
		<cfargument name="isASC" type="any" required="false" default="true">
		
		<cfset var qGet = ''>
		<cfquery name="qGet" datasource="#session.datasource#">
			SELECT *
			  FROM [#obj.tableName#]
			  WHERE [#obj.parentFK#] = <cfqueryparam value="#arguments.parentFK#">
			  <cfif isDefined("arguments.orderBy") and len(arguments.orderBy) gt 1>
					Order BY [#arguments.orderBy#]
					<cfif arguments.isASC>
						ASC
					<cfelse>
						DESC
					</cfif>
			  </cfif>
		</cfquery>
		
		<cfreturn qGet>
	</cffunction>
	
	<cffunction name="insert" access="public" output="false" returntype="string" hint="Insert a new record and return the new ID">
		<cfargument name="newObj" type="entities._AbstractEntity" required="true">
		<cfargument name="newID" type="string" required="false" default="" hint="if set will be used as the PK GUID otherwise a NEWID() will be generated">
		<!--- get all columns from database table --->
		
		<cfset var tableMata = GetTableInfo()>
		
		<cfset var qInsert = ''>
		<cfquery name="qInsert" datasource="#session.datasource#">
			DECLARE @GUID uniqueidentifier
			SET @GUID = <cfif arguments.newID NEQ ''>'#arguments.newID#'<cfelse>NEWID()</cfif>
			INSERT INTO #obj.tableName#
					(
					[#obj.tablePK#]
					<cfloop array="#tableMata#" index="columnStruct">
						<!--- not the primary Key--->
						<cfif columnStruct.Name neq obj.tablePK>
							,[#columnStruct.Name#]
						</cfif>
					</cfloop>
					)
			VALUES
				(
				@GUID
				
				<cfloop array="#tableMata#" index="columnStruct">
					<cfif columnStruct.Name neq obj.tablePK>
						
						<cfset var attrValue = ''>
						<cftry>
							<cfset attrValue = #arguments.newObj['#columnStruct.Name#']#>
							<cfcatch></cfcatch>
						</cftry>
						<cfif len(attrValue) gt 0>
								<cfswitch expression="#columnStruct.TypeName#">
									<!--- bigint --->
									<cfcase value="bigint">
										,<cfqueryparam cfsqltype="cf_sql_bigint" value="#attrValue#">	
									</cfcase>
									<!--- binary --->
									<cfcase value="binary">
										,<cfqueryparam cfsqltype="cf_sql_blob" value="#attrValue#">	
									</cfcase>
									
									<!--- bit --->
									<cfcase value="bit">
										,<cfqueryparam cfsqltype="cf_sql_bit" value="#attrValue#">	
									</cfcase>
									<!--- char --->
									<cfcase value="char">
										,<cfqueryparam cfsqltype="cf_sql_char" value="#attrValue#">	
									</cfcase>
									<!--- date --->
									<cfcase value="date">
										,<cfqueryparam cfsqltype="cf_sql_date" value="#attrValue#">	
									</cfcase>
									<!--- datetime --->
									<cfcase value="datetime">
										,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#attrValue#">	
									</cfcase>
									<!--- datetime2 --->
									<cfcase value="datetime2">
										,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#attrValue#">	
									</cfcase>
									<!--- datetimeoffset --->
									<cfcase value="datetimeoffset">
										,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#attrValue#">	
									</cfcase>
									<!--- decimal --->
									<cfcase value="decimal">
										,<cfqueryparam cfsqltype="cf_sql_decimal" value="#attrValue#">	
									</cfcase>
									<!--- float --->
									<cfcase value="float">
										,<cfqueryparam cfsqltype="cf_sql_float" value="#attrValue#">	
									</cfcase>
									<!--- geography --->
									<cfcase value="geography">
										,<cfqueryparam value="#attrValue#">	
									</cfcase>
									<!--- geometry --->
									<cfcase value="geometry">
										,<cfqueryparam value="#attrValue#">	
									</cfcase>
									<!--- hierarchyid --->
									<cfcase value="hierarchyid">
										,<cfqueryparam value="#attrValue#">	
									</cfcase>
									
									<!--- image --->
									<cfcase value="image">
										,<cfqueryparam cfsqltype="cf_sql_blob" value="#attrValue#">	
									</cfcase>
									
									<!--- int --->
									<cfcase value="int">
										,<cfqueryparam cfsqltype="cf_sql_integer" value="#attrValue#">	
									</cfcase>
									
									<!--- money --->
									<cfcase value="money">
										,<cfqueryparam cfsqltype="cf_sql_money" value="#attrValue#">	
									</cfcase>
									
									<!--- nchar --->
									<cfcase value="nchar">
										,<cfqueryparam cfsqltype="cf_sql_char" value="#attrValue#">	
									</cfcase>
									
									<!--- ntext --->
									<cfcase value="ntext">
										,<cfqueryparam cfsqltype="cf_sql_varchar" value="#attrValue#">	
									</cfcase>
									
									<!--- numeric --->
									<cfcase value="numeric">
										,<cfqueryparam cfsqltype="cf_sql_numeric" value="#attrValue#">	
									</cfcase>
									
									<!--- nvarchar --->
									<cfcase value="nvarchar">
										,<cfqueryparam cfsqltype="cf_sql_varchar" value="#attrValue#">	
									</cfcase>
									
									<!--- nvarchar(max) --->
									<cfcase value="nvarchar(max)">
										,<cfqueryparam cfsqltype="cf_sql_varchar" value="#attrValue#">	
									</cfcase>
									
									<!--- real --->
									<cfcase value="real">
										,<cfqueryparam cfsqltype="cf_sql_real" value="#attrValue#">	
									</cfcase>
									
									<!--- smalldatetime --->
									<cfcase value="smalldatetime">
										,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#attrValue#">	
									</cfcase>
									
									<!--- smallint --->
									<cfcase value="smallint">
										,<cfqueryparam cfsqltype="cf_sql_smallint" value="#attrValue#">	
									</cfcase>
									
									<!--- smallmoney --->
									<cfcase value="smallmoney">
										,<cfqueryparam cfsqltype="cf_sql_money" value="#attrValue#">	
									</cfcase>
									
									<!--- sql_variant --->
									<cfcase value="sql_variant">
										,<cfqueryparam cfsqltype="cf_sql_varchar" value="#attrValue#">	
									</cfcase>
									
									<!--- text --->
									<cfcase value="text">
										,<cfqueryparam cfsqltype="cf_sql_varchar" value="#attrValue#">	
									</cfcase>
									
									<!--- time --->
									<cfcase value="time">
										,<cfqueryparam cfsqltype="cf_sql_time" value="#attrValue#">	
									</cfcase>
									
									<!--- timestamp --->
									<cfcase value="timestamp">
										,<cfqueryparam cfsqltype="cf_sql_timestamp" value="#attrValue#">	
									</cfcase>
									
									<!--- tinyint --->
									<cfcase value="tinyint">
										,<cfqueryparam cfsqltype="cf_sql_tinyint" value="#attrValue#">	
									</cfcase>
									
									<!--- uniqueidentifier --->
									<cfcase value="uniqueidentifier">
										,<cfqueryparam cfsqltype="cf_sql_varchar" value="#attrValue#">	
									</cfcase>
									
									<!--- varbinary --->
									<cfcase value="varbinary">
										,<cfqueryparam cfsqltype="cf_sql_blob" value="#attrValue#">	
									</cfcase>

									<!--- varbinary(max) --->
									<cfcase value="varbinary(max)">
										,<cfqueryparam cfsqltype="cf_sql_blob" value="#attrValue#">	
									</cfcase>


									<!--- varchar --->
									<cfcase value="varchar">
										,<cfqueryparam cfsqltype="cf_sql_varchar" value="#attrValue#">	
									</cfcase>
									
									<cfcase value="varchar(max)">
										,<cfqueryparam cfsqltype="cf_sql_varchar" value="#attrValue#">	
									</cfcase>
									
									<!--- xml --->
									<cfcase value="xml">
										,<cfqueryparam cfsqltype="cf_sql_varchar" value="#attrValue#">	
									</cfcase>
									
									<cfdefaultcase>
										,<cfqueryparam value="#attrValue#">	
									</cfdefaultcase>
								</cfswitch>
						<cfelse>
							,null
						</cfif>
						
						
					</cfif>
				</cfloop>
				
				)
			SELECT @GUID AS newID
		</cfquery>
		
		<!--- Retrun new ID--->
		<cfreturn qInsert.newID>
	</cffunction>
		
	<cffunction name="update" access="public" output="false" returntype="struct" hint="">
		<cfargument name="updateObject" type="entities._AbstractEntity" required="true">
		<cfset var returnVal = structNew()>
		<cfset returnVal["success"] = false>
		<cfset returnVal["message"] = ''>
		<cftry>
			<cfset var tableMata = GetTableInfo()>
			<cfset var tableID = #arguments.updateObject['#obj.tablePK#']#>
			<cfquery name="qupdate" datasource="#session.datasource#">
				UPDATE [#obj.tableName#]
				 SET 
				  [#obj.tablePK#] = <cfqueryparam value="#tableID#">
					<cfloop array="#tableMata#" index="columnStruct">
						
						<cfif columnStruct.Name neq obj.tablePK>
							<cfset attrValue = ''>
							<cftry>
								<cfset attrValue = #arguments.updateObject['#columnStruct.Name#']#>
								<cfcatch>
									<cfset attrValue = ''>
								</cfcatch>
							</cftry>
							<cfif len(attrValue) gt 0>
									
									<cfswitch expression="#columnStruct.TypeName#">
									<!--- bigint --->
									<cfcase value="bigint">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_bigint" value="#attrValue#">	
									</cfcase>
									<!--- binary --->
									<cfcase value="binary">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_blob" value="#attrValue#">	
									</cfcase>
									<!--- bit --->
									<cfcase value="bit">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_bit" value="#attrValue#">	
									</cfcase>
									<!--- char --->
									<cfcase value="char">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_char" value="#attrValue#">	
									</cfcase>
									<!--- date --->
									<cfcase value="date">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_date" value="#attrValue#">	
									</cfcase>
									<!--- datetime --->
									<cfcase value="datetime">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_timestamp" value="#attrValue#">	
									</cfcase>
									<!--- datetime2 --->
									<cfcase value="datetime2">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_timestamp" value="#attrValue#">	
									</cfcase>
									<!--- datetimeoffset --->
									<cfcase value="datetimeoffset">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_timestamp" value="#attrValue#">	
									</cfcase>
									<!--- decimal --->
									<cfcase value="decimal">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_decimal" value="#attrValue#">	
									</cfcase>
									<!--- float --->
									<cfcase value="float">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_float" value="#attrValue#">	
									</cfcase>
									<!--- geography --->
									<cfcase value="geography">
										,[#columnStruct.Name#] = <cfqueryparam value="#attrValue#">	
									</cfcase>
									<!--- geometry --->
									<cfcase value="geometry">
										,[#columnStruct.Name#] = <cfqueryparam value="#attrValue#">	
									</cfcase>
									<!--- hierarchyid --->
									<cfcase value="hierarchyid">
										,[#columnStruct.Name#] = <cfqueryparam value="#attrValue#">	
									</cfcase>
									
									<!--- image --->
									<cfcase value="image">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_blob" value="#attrValue#">	
									</cfcase>
									
									<!--- int --->
									<cfcase value="int">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_integer" value="#attrValue#">	
									</cfcase>
									
									<!--- money --->
									<cfcase value="money">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_money" value="#attrValue#">	
									</cfcase>
									
									<!--- nchar --->
									<cfcase value="nchar">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_char" value="#attrValue#">	
									</cfcase>
									
									<!--- ntext --->
									<cfcase value="ntext">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#attrValue#">	
									</cfcase>
									
									<!--- numeric --->
									<cfcase value="numeric">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_numeric" value="#attrValue#">	
									</cfcase>
									
									<!--- nvarchar --->
									<cfcase value="nvarchar">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#attrValue#">	
									</cfcase>
									
									<!--- nvarchar(max) --->
									<cfcase value="nvarchar(max)">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#attrValue#">	
									</cfcase>
									
									<!--- real --->
									<cfcase value="real">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_real" value="#attrValue#">	
									</cfcase>
									
									<!--- smalldatetime --->
									<cfcase value="smalldatetime">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_timestamp" value="#attrValue#">	
									</cfcase>
									
									<!--- smallint --->
									<cfcase value="smallint">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_smallint" value="#attrValue#">	
									</cfcase>
									
									<!--- smallmoney --->
									<cfcase value="smallmoney">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_money" value="#attrValue#">	
									</cfcase>
									
									<!--- sql_variant --->
									<cfcase value="sql_variant">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#attrValue#">	
									</cfcase>
									
									<!--- text --->
									<cfcase value="text">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#attrValue#">	
									</cfcase>
									
									<!--- time --->
									<cfcase value="time">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_time" value="#attrValue#">	
									</cfcase>
									
									<!--- timestamp --->
									<cfcase value="timestamp">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_timestamp" value="#attrValue#">	
									</cfcase>
									
									<!--- tinyint --->
									<cfcase value="tinyint">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_tinyint" value="#attrValue#">	
									</cfcase>
									
									<!--- uniqueidentifier --->
									<cfcase value="uniqueidentifier">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#attrValue#">	
									</cfcase>
									
									<!--- varbinary --->
									<cfcase value="varbinary">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_blob" value="#attrValue#">	
									</cfcase>

									<!--- varbinary(max) --->
									<cfcase value="varbinary(max)">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_blob" value="#attrValue#">	
									</cfcase>
									
									<!--- varchar --->
									<cfcase value="varchar">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#attrValue#">	
									</cfcase>
									
									<!--- varchar(max) --->
									<cfcase value="varchar(max)">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#attrValue#">	
									</cfcase>

									<!--- xml --->
									<cfcase value="xml">
										,[#columnStruct.Name#] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#attrValue#">	
									</cfcase>
									<cfdefaultcase>
										,[#columnStruct.Name#] = <cfqueryparam value="#attrValue#">	
									</cfdefaultcase>
								</cfswitch>
							<cfelse>
								,[#columnStruct.Name#] = NULL
							</cfif>
						</cfif>
					</cfloop>
				WHERE [#obj.tablePK#] = <cfqueryparam value="#tableID#">
				AND 1 = 1
			</cfquery>
			<cfset returnVal["success"] = true>
		<cfcatch>
			<cfmail to="debug@layerx.co.nz" from="debug@layerx.co.nz" subject="COMS Mobile Error - DAO update method" type="html"><cfdump var="#cfcatch#"></cfmail>
			<cfset returnVal["success"] = false>
			<cfset returnVal["message"] = cfcatch.detail>
		</cfcatch>
		</cftry>
		
		<cfreturn returnVal>
	</cffunction>

	<cffunction name="updateAttributes" access="public" output="false" returntype="struct" hint="">
		<cfargument name="tableID" type="String" required="true">
		<cfargument name="attributeList" type="array" required="true">
		<cfargument name="valueList" type="array" required="true">
		
		<cfset var returnVal = structNew()>
		<cfset returnVal["success"] = false>
		<cfset returnVal["message"] = ''>
		
		<!--- Attributes List should same as Value list --->
		<cfif Arraylen(arguments.attributeList) eq Arraylen(arguments.valueList)>
			<cfset var numberAttr =  Arraylen(arguments.attributeList)>

			<cfquery name="qupdate" datasource="#session.datasource#">
				UPDATE [#obj.tableName#]
				SET 
				  <cfloop from="1" to="#numberAttr#" index="i">
					<cfif i neq 1>,</cfif>[#attributeList[i]#] = <cfqueryparam value="#arguments.valueList[i]#">
				  </cfloop>
				WHERE [#obj.tablePK#] = <cfqueryparam value="#arguments.tableID#">
			</cfquery>
			<cfset returnVal["success"] = true>
				
		<cfelse>
			<cfset returnVal["success"] = false>
			<cfset returnVal["message"] = 'AttributeList length not mactch valuelist'>
		</cfif>
		
		<cfreturn returnVal>
	</cffunction>

	<cffunction name="deleteByID" access="remote" output="false" returntype="struct">
		<cfargument name="tableID" type="string" required="yes" default="">
		
		<cfset var returnVal = structNew()>
		<cfset returnVal["success"] = false>
		<cfset returnVal["message"] = ''>
		
		<cftry>
			<cfquery name="qDelete" datasource="#session.datasource#">
				DELETE FROM [#obj.tableName#]
					WHERE [#obj.tablePK#] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.tableID#" />
			</cfquery>
			<cfset returnVal["success"] = true>
		<cfcatch>
			<cfset returnVal["success"] = false>
			<cfset returnVal["message"] = cfcatch.detail>
		</cfcatch>
		</cftry>
		<cfreturn returnVal />
	</cffunction>
	
	<cffunction name="listFields" access="public" output="false" returntype="Any">
		
		<cfquery name="qData" datasource="#session.datasource#">
			SELECT top 1 * FROM [#obj.tableName#]
		</cfquery>
		<cfreturn qData.ColumnList />
		
	</cffunction>
	
	<cffunction name="GetTableInfo" access="public" output="false" returntype="Any">
		<cfquery name="qData" datasource="#session.datasource#">
			SELECT top 1 * FROM [#obj.tableName#]
		</cfquery>
		<cfset var tableMeta = GetMetaData(qData)>
		<cfreturn tableMeta>
	</cffunction>
	
</cfcomponent>