<!--- ///////////////////////////////////////////////// --->
<!--- ///////////////////////////////////////////
/////////////////////////////////////////////////
/////////////////////////////////////////////////

This page will generate a Database Access Object (DAO).

1. TableName: should be the database table name.
2. isOverwrite: if true, it will delete the existing entity and create a new one.

/////////////////////////////////////////// --->
<cfset tableName = URL.tableName /> <!--- required --->
<cfset isOverwrite = URL.isOverwrite /> <!--- optional --->

<h2>DAO</h2>

<cfif len(tableName) lte 1>
	<h2>TableName is required !!</h2>
	<cfabort />
</cfif>

<cfset filePath = expandPath( "./#tableName#DAO.cfc" ) />

<!---
	Delete the file if it exists so that we don't keep populating
	the same document.
	--->
<cfif fileExists( filePath )>
	<cfif isOverwrite>
		<cfset fileDelete( filePath ) />
	<cfelse>
		<cfoutput>
			<div style="text-align: left; font-size: 20px;">
				<p>
					1. <strong>#tableName#.cfc</strong> has already existed. 
				</p>
				<p>
					2. Please set <<strong style="color: red;">Overwrite = true</strong>> if you want overwrite the existing DAO.
				</p>
			</div>
		</cfoutput>
		<cfabort>
	</cfif>
	
</cfif>

<!---
	Create a file file object that we can write to. We have to
	explicitly define the Mode as Append otherwise the file will
	be opened on read mode.
	--->
<cfset dataFile = fileOpen( filePath, "append" ) />
<!--- Header of the class --->
<cfset fileWriteLine(dataFile,'<cfcomponent  extends="dao._AbstractDAO">
') />
<cfoutput>
	<cfset fileWriteLine(dataFile,'') />
	<cfset fileWriteLine(dataFile,'	<!--- This class is created by [ReverseEngine] on #now()#----> ') />
	<cfset fileWriteLine(dataFile,'') />
</cfoutput>

<cfset fileWriteLine(dataFile,'') />
<cfset fileWriteLine(dataFile,'	<!--- init current model, so the model know what DB table it talks to ---> ') />
<cfset fileWriteLine(dataFile,'') />
<!--- Init part --->
<cfset fileWriteLine(dataFile,'
	<cffunction name="init">') />
<cfset fileWriteLine(dataFile,'		<cfset obj = new entities.#tableName#()>') />
<cfset fileWriteLine(dataFile,'	</cffunction>') />
<cfset fileWriteLine(dataFile,'') />
<cfset fileWriteLine(dataFile,'') />
<cfset fileWriteLine(dataFile,'</cfcomponent>') />
<!---
	Now that we have finished writing the file, let's close it.
	This will close the stream which should prevent any unintented
	locking on the file access.
	--->
<cfset fileClose( dataFile ) />
<cfoutput><strong>
		#tableName#DAO.cfc
	</strong>
	has been created.</cfoutput>
