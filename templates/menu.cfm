

<div class="text-center w-100 p-y-3">
    <a class="" href="#/"><img src="img/logo.svg" width="150"></a>
    <div class="p-y-1"><h4 class="text-primary">Welcome</h4></div>
</div>

<ul class="list-group">
    <a href="#/home" aside-menu-toggle="menu-1" class="list-group-item p-y-2"><i class="fa fa-fw fa-lg fa-home"></i> Available Work Orders</a>
    <a href="#/search" aside-menu-toggle="menu-1" class="list-group-item p-y-2"><i class="fa fa-fw fa-lg fa-search"></i> Search</a>

    <!--- @TODO Remove below options--->
  <!---  <li class="list-group-item disabled">DEBUG (in order of appearance)</li>
    <a href="#/work-order-tasks" aside-menu-toggle="menu-1" class="list-group-item p-y-2">Work Order Tasks</a>
    <a href="#/relocate" aside-menu-toggle="menu-1" class="list-group-item p-y-2">Relocate Machine</a>
    <a href="#/remove" aside-menu-toggle="menu-1" class="list-group-item p-y-2">Remove Machine</a>
    <a href="#/install" aside-menu-toggle="menu-1" class="list-group-item p-y-2">Install Machine</a>
    <a href="#/confirmation" aside-menu-toggle="menu-1" class="list-group-item p-y-2">Summary</a>--->
    <!--- @TODO End Debug options --->
</ul>

<div ng-include='"templates/footer.html"'></div>