<cfcomponent extends="cfc.Global">
	<!--- GET ONE --->
	<cffunction name="getJobDetail" access="public" returntype="query" output="no">
		<cfargument name="jobID" type="string" required="yes">

		<cfset var data = "">
		<cfquery name="data" datasource="#session.datasource#">
			SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

			SELECT [jobID],[jobName],[jobCreatedBy],[jobStatus],[jobPriority],[jobSLA],[jobSLATimer],[jobType],[jobDescription],
					[jobCreationDate],[jobLastUpdated],[jobACCNO],[NAME] AS [jobAccName],[jobSEQNO],[jobNumber],[jobMessageID],[jobEmailTo],[jobEmailFrom],[jobAssignedTo],
					[jobRecentUpdate],[jobScheduledDate],[jobArrivedOnSite],[jobCompleteStamp],[jobCallID],[jobTimeStamp],[jobReference],[jobFlag],[timeToOnSite],[timeToCompleted],
					[jobEffort],[includeInReports],[exchangeUID_task],[exchangeUID_calendar], [isFromWorkOrder],[jobNotes],[jobTypeName]
			FROM [smeJobs] INNER JOIN [ClientAccounts] ON [jobACCNO] = [accno]
			INNER JOIN [lib_jobType] ON [lib_jobType].[jobTypeID] = [smeJobs].[jobType]
			WHERE	jobID = <cfqueryparam cfsqltype="cf_sql_varchar" maxlength="36" value="#arguments.jobID#">
            AND		jobDeleted = 0
		</cfquery>
		<cfreturn data>
	</cffunction>

	<cffunction name="getByJobNumber" access="remote" returnType="struct" output="false" returnformat="JSON">
		<cfargument name="jobNumber" type="any" required="true" default="0">
		<cfset var returnVal = structNew()>
		<cfset returnVal["success"] = true>
		<cfset returnVal["message"] = ''>
		<cfset returnVal["keyword"] = 'Job Number : ' &arguments.jobNumber>
		<cfset returnVal["workOrderJobs"] = arrayNew(1)>

	
		<cfif not isdefined("arguments.jobNumber") or  not isNumeric(arguments.jobNumber) or arguments.jobNumber lte 0 >
			<!---the Job Number is not valid, just return an empty array--->
			<cfreturn returnVal>
		</cfif>

		<cfset var qGet = ''>
		<cfquery name="qGet" datasource="#session.datasource#">
            SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
            SELECT top 1000 smeJobs.jobID, smeJobs.jobName, smeJobs.jobCreatedBy, smeJobs.jobStatus, smeJobs.jobPriority, smeJobs.jobSLA, smeJobs.jobSLATimer,
								  smeJobs.jobType, smeJobs.jobDescription, smeJobs.jobCreationDate, smeJobs.jobLastUpdated, smeJobs.jobACCNO, smeJobs.jobSEQNO, smeJobs.jobNumber,
								  smeJobs.jobEmailTo, smeJobs.jobEmailFrom, smeJobs.jobAssignedTo, smeJobs.jobScheduledDate, smeJobs.jobArrivedOnSite, smeJobs.jobCompleteStamp, smeJobs.timeToOnSite, smeJobs.timeToCompleted, smeJobs.jobEffort,
								  smeJobs.jobReference, ClientAccounts.NAME, smeJobs.jobMessageID, smeJobs.jobRecentUpdate, smeJobs.jobTimeStamp, smeJobs.jobFlag, smeJobs.exchangeUID_task, smeJobs.exchangeUID_calendar, smeJobs.isFromWorkOrder
				FROM smeJobs INNER JOIN ClientAccounts ON smeJobs.jobACCNO = ClientAccounts.ACCNO
				WHERE 1=1
				AND	jobDeleted = 0
				AND jobNumber = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.jobNumber#">
                ORDER BY jobCreationDate DESC
        </cfquery>

		<cfset var newJobArray = arrayNew(1)>
		<cfset var startedJobArray = arrayNew(1)>
		<cfset var completedJobArray = arrayNew(1)>

		<cfloop query="qGet">
			<cfset woDao = new workorder()>
			<cfset var qJobs = woDao.getAllByJobID(qGet.jobID)>
			<cfif isQuery(qJobs) and qJobs.recordCount gt 0>
				<!---there is an machine job--->
				<cfset var aJobRecord = structNew()>
				<cfset ajobrecord["jobID"] = qGet.jobID>
				<cfset ajobrecord["jobName"] = ReplaceNoCase(qGet.jobName, 'Work Order: ', '', 'all')>
				<cfset ajobrecord["jobNumber"] = qGet.jobNumber>

				<cfset var totalAction = qJobs.recordcount>
				<cfset var completedAction = 0>
				<cfset var lastActionCompletedDate = now()>
				<cfloop query="qJobs">
					<cfif IsActionCompleted eq 1>
						<cfset completedAction = completedAction +1>
						<cfif DateCompare(qJobs.ActionCompletedOn, lastActionCompletedDate) eq -1> <!---ActionCompletedOn earlier than lastActionCompletedDate--->
							<cfset lastActionCompletedDate =  qJobs.ActionCompletedOn>
						</cfif>
					</cfif>
				</cfloop>

				<cfif completedAction neq 0 and totalAction eq completedAction>
<!--- all completed, check the last complete date.  --->
					<cfif DateDiff("d", lastActionCompletedDate, now()) gt 3 or DateDiff("d", lastActionCompletedDate, now()) lt -3>
						<cfcontinue>
					</cfif>
				</cfif>

				<cfset ajobrecord["completedAction"] = completedAction>
				<cfset ajobrecord["totalAction"] = totalAction>

				<cfif completedAction eq 0>
					<cfset ajobrecord["status"] = 'new'>
					<cfset ajobrecord["class"] = 'label-success'>
					<cfset arrayappend(newJobArray, ajobrecord)>
				<cfelseif  completedAction neq totalAction>
					<cfset ajobrecord["status"] = 'started'>
					<cfset ajobrecord["class"] = 'label-warning'>
					<cfset arrayappend(startedJobArray, ajobrecord)>
				<cfelse>
					<cfset ajobrecord["status"] = 'completed'>
					<cfset ajobrecord["class"] = 'label-info'>
					<cfset arrayappend(completedJobArray, ajobrecord)>
				</cfif>
			</cfif>
		</cfloop>

		<cfset var utils = new cfc.Utils()>
		<cfset returnVal["workOrderJobs"] = utils.arrayAppendAll(returnVal["workOrderJobs"], startedJobArray)>
		<cfset returnVal["workOrderJobs"] = utils.arrayAppendAll(returnVal["workOrderJobs"], newJobArray)>
		<cfset returnVal["workOrderJobs"] = utils.arrayAppendAll(returnVal["workOrderJobs"], completedJobArray)>

		<cfreturn returnVal />
	</cffunction>

	<cffunction name="getByWorkOrderNumber" access="remote" returnType="struct" output="false" returnformat="JSON">
		<cfargument name="workOrderNumber" type="any" required="true" default="0">
		<cfset var returnVal = structNew()>
		<cfset returnVal["success"] = true>
		<cfset returnVal["message"] = ''>
		<cfset returnVal["keyword"] = "WO Number: " & arguments.workOrderNumber>

		<cfset returnVal["workOrderJobs"] = arrayNew(1)>

		<cfif not isdefined("arguments.workOrderNumber") or  not isNumeric(arguments.workOrderNumber) or arguments.workOrderNumber lte 0 >
<!---the Job Number is not valid, just return an empty array--->
			<cfreturn returnVal>
		</cfif>

		<cfset var qGet = ''>
		<cfquery name="qGet" datasource="#session.datasource#">
            SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
				SELECT TOP 1000 [jobID]
					  ,[jobName]
					  ,[jobNumber]
				  FROM [smeJobs]
				  INNER JOIN [WorkOrder_Job_Mapping] on [smeJobs].jobID = [WorkOrder_Job_Mapping].jobFK
				  INNER JOIN [workOrder] on [workOrder].ID = [WorkOrder_Job_Mapping].workOrderFK
				  WHERE [workOrder].Number = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.workOrderNumber#">
        </cfquery>

		<cfset var newJobArray = arrayNew(1)>
		<cfset var startedJobArray = arrayNew(1)>
		<cfset var completedJobArray = arrayNew(1)>

		<cfloop query="qGet">
			<cfset woDao = new workorder()>
			<cfset var qJobs = woDao.getAllByJobID(qGet.jobID)>
			<cfif isQuery(qJobs) and qJobs.recordCount gt 0>
<!---there is an machine job--->
				<cfset var aJobRecord = structNew()>
				<cfset ajobrecord["jobID"] = qGet.jobID>
				<cfset ajobrecord["jobName"] = ReplaceNoCase(qGet.jobName, 'Work Order: ', '', 'all')>
				<cfset ajobrecord["jobNumber"] = qGet.jobNumber>

				<cfset var totalAction = qJobs.recordcount>
				<cfset var completedAction = 0>
				<cfset var lastActionCompletedDate = now()>
				<cfloop query="qJobs">
					<cfif IsActionCompleted eq 1>
						<cfset completedAction = completedAction +1>
						<cfif DateCompare(qJobs.ActionCompletedOn, lastActionCompletedDate) eq -1> <!---ActionCompletedOn earlier than lastActionCompletedDate--->
							<cfset lastActionCompletedDate =  qJobs.ActionCompletedOn>
						</cfif>
					</cfif>
				</cfloop>

				<cfif completedAction neq 0 and totalAction eq completedAction>
<!--- all completed, check the last complete date.  --->
					<cfif DateDiff("d", lastActionCompletedDate, now()) gt 3 or DateDiff("d", lastActionCompletedDate, now()) lt -3>
						<cfcontinue>
					</cfif>
				</cfif>

				<cfset ajobrecord["completedAction"] = completedAction>
				<cfset ajobrecord["totalAction"] = totalAction>

				<cfif completedAction eq 0>
					<cfset ajobrecord["status"] = 'new'>
					<cfset ajobrecord["class"] = 'label-success'>
					<cfset arrayappend(newJobArray, ajobrecord)>
					<cfelseif  completedAction neq totalAction>
					<cfset ajobrecord["status"] = 'started'>
					<cfset ajobrecord["class"] = 'label-warning'>
					<cfset arrayappend(startedJobArray, ajobrecord)>
				<cfelse>
					<cfset ajobrecord["status"] = 'completed'>
					<cfset ajobrecord["class"] = 'label-info'>
					<cfset arrayappend(completedJobArray, ajobrecord)>
				</cfif>
			</cfif>
		</cfloop>

		<cfset var utils = new cfc.Utils()>
		<cfset returnVal["workOrderJobs"] = utils.arrayAppendAll(returnVal["workOrderJobs"], startedJobArray)>
		<cfset returnVal["workOrderJobs"] = utils.arrayAppendAll(returnVal["workOrderJobs"], newJobArray)>
		<cfset returnVal["workOrderJobs"] = utils.arrayAppendAll(returnVal["workOrderJobs"], completedJobArray)>

		<cfreturn returnVal />
	</cffunction>


	<cffunction name="getByVenueGMVID" access="remote" returnType="struct" output="false" returnformat="JSON">
		<cfargument name="venueGMVID" type="any" required="true" default="0">
		<cfset var returnVal = structNew()>
		<cfset returnVal["success"] = true>
		<cfset returnVal["message"] = ''>
		<cfset returnVal["keyword"] = "GMVID: " &arguments.venueGMVID>
		<cfset returnVal["workOrderJobs"] = arrayNew(1)>

		<cfif not isdefined("arguments.venueGMVID") or  not isNumeric(arguments.venueGMVID) or arguments.venueGMVID lte 0 >
<!---the Job Number is not valid, just return an empty array--->
			<cfreturn returnVal>
		</cfif>

		<cfset var qGet = ''>
		<cfquery name="qGet" datasource="#session.datasource#">
            SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
				SELECT TOP 1000 [jobID]
					  ,[jobName]
					  ,[jobNumber]
				  FROM [smeJobs]
				  INNER JOIN [WorkOrder_Job_Mapping] on [smeJobs].jobID = [WorkOrder_Job_Mapping].jobFK
				  INNER JOIN [workOrder] on [workOrder].ID = [WorkOrder_Job_Mapping].workOrderFK
				  INNER JOIN [ClientAccounts] on [ClientAccounts].ACCNO =  [workOrder].accnoFK
				  WHERE [ClientAccounts].GMVID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.venueGMVID#">
		</cfquery>

		<cfset var newJobArray = arrayNew(1)>
		<cfset var startedJobArray = arrayNew(1)>
		<cfset var completedJobArray = arrayNew(1)>

		<cfloop query="qGet">
			<cfset woDao = new workorder()>
			<cfset var qJobs = woDao.getAllByJobID(qGet.jobID)>
			<cfif isQuery(qJobs) and qJobs.recordCount gt 0>
<!---there is an machine job--->
				<cfset var aJobRecord = structNew()>
				<cfset ajobrecord["jobID"] = qGet.jobID>
				<cfset ajobrecord["jobName"] = ReplaceNoCase(qGet.jobName, 'Work Order: ', '', 'all')>
				<cfset ajobrecord["jobNumber"] = qGet.jobNumber>

				<cfset var totalAction = qJobs.recordcount>
				<cfset var completedAction = 0>
				<cfset var lastActionCompletedDate = now()>
				<cfloop query="qJobs">
					<cfif IsActionCompleted eq 1>
						<cfset completedAction = completedAction +1>
						<cfif DateCompare(qJobs.ActionCompletedOn, lastActionCompletedDate) eq -1> <!---ActionCompletedOn earlier than lastActionCompletedDate--->
							<cfset lastActionCompletedDate =  qJobs.ActionCompletedOn>
						</cfif>
					</cfif>
				</cfloop>

				<cfif completedAction neq 0 and totalAction eq completedAction>
<!--- all completed, check the last complete date.  --->
					<cfif DateDiff("d", lastActionCompletedDate, now()) gt 3 or DateDiff("d", lastActionCompletedDate, now()) lt -3>
						<cfcontinue>
					</cfif>
				</cfif>

				<cfset ajobrecord["completedAction"] = completedAction>
				<cfset ajobrecord["totalAction"] = totalAction>

				<cfif completedAction eq 0>
					<cfset ajobrecord["status"] = 'new'>
					<cfset ajobrecord["class"] = 'label-success'>
					<cfset arrayappend(newJobArray, ajobrecord)>
					<cfelseif  completedAction neq totalAction>
					<cfset ajobrecord["status"] = 'started'>
					<cfset ajobrecord["class"] = 'label-warning'>
					<cfset arrayappend(startedJobArray, ajobrecord)>
				<cfelse>
					<cfset ajobrecord["status"] = 'completed'>
					<cfset ajobrecord["class"] = 'label-info'>
					<cfset arrayappend(completedJobArray, ajobrecord)>
				</cfif>
			</cfif>
		</cfloop>

		<cfset var utils = new cfc.Utils()>
		<cfset returnVal["workOrderJobs"] = utils.arrayAppendAll(returnVal["workOrderJobs"], startedJobArray)>
		<cfset returnVal["workOrderJobs"] = utils.arrayAppendAll(returnVal["workOrderJobs"], newJobArray)>
		<cfset returnVal["workOrderJobs"] = utils.arrayAppendAll(returnVal["workOrderJobs"], completedJobArray)>

		<cfreturn returnVal />
	</cffunction>

	<cffunction name="getSuppliersWorkOrderJobs" access="remote" returnType="struct" output="false" returnformat="JSON">
		<cfargument name="searchValue" type="any" required="true"  >
		<cfargument name="searchBy" type="any" required="true" >
		<cfargument name="isAreaManager" type="any" required="true" default="0">

		<cfset var returnVal = structNew()>
		<cfset returnVal["success"] = true>
		<cfset returnVal["message"] = ''>
		<cfset var searchText = #Replace(LCase(arguments.searchBy)," ","")#>
		<cfset var jobText = "jobnumber">
		<cfset var workOrderText = "worknumber">
		<cfset returnVal["workOrderJobs"] = arrayNew(1)>
		<cfset returnVal["keyword"] =  arguments.searchBy & ' : ' & arguments.searchValue>
		
		<cfif not isdefined("arguments.searchValue") or not isNumeric(arguments.searchValue) or arguments.searchValue lte 0 >
			<cfreturn returnVal>
		</cfif>
		
		<cfset var qGet = ''>
		<cfquery name="qGet" datasource="#session.datasource#">
            SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
			SELECT JobNumber, JobID, GMVID, jobName, jobNumber, Number 
		  		,SUM(cast(IsActionCompleted as int)) AS 'Completed'
			   ,COUNT(IsActionCompleted) 'Total'
		  	FROM (
				  SELECT distinct 
				  sj.JobNumber,sj.[jobID],ClientAccounts.GMVID,[jobName],workOrder.Number 
				  ,ISNULL([IsActionCompleted],0) IsActionCompleted
				  ,[workOrderSub_MachineItem].ID --need to get each "todo item"
				  FROM [smeJobs] sj
				  INNER JOIN [WorkOrder_Job_Mapping] on sj.jobID = [WorkOrder_Job_Mapping].jobFK
				  INNER JOIN [workOrder] on [workOrder].ID = [WorkOrder_Job_Mapping].workOrderFK
				  INNER JOIN [ClientAccounts] on [ClientAccounts].ACCNO =  [workOrder].accnoFK
				  --Below Inner joins for Suppliers related work Orders
				  INNER JOIN [workOrderSub_Machine] wos_m on [workOrder].ID =   wos_m.WorkOrderFK 
				  INNER JOIN [workOrderSub_MachineItem]  on wos_m.ID = [workOrderSub_MachineItem].MachineFK
                  INNER JOIN [workOrder_lib_machine_Action] on [workOrderSub_MachineItem].ActionFK = workOrder_lib_machine_Action.ID
				  INNER JOIN [workOrder_GreatPlain] wg on sj.jobID =  wg.JobID and wg.WorkOrderID = workOrder.ID   and [workOrderSub_MachineItem].WarehouseFK = wg.SupplierID
				  WHERE 
					<cfif searchText eq jobText>
				  	 	jobNumber = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.searchValue#">
						AND	jobDeleted = 0
					<cfelseif searchText eq workOrderText >
				  	 	workOrder.Number = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.searchValue#">
					<cfelse>
				  	 	[ClientAccounts].GMVID = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.searchValue#">
					</cfif>
					<cfif arguments.isAreaManager eq 1>
						And [ClientAccounts].[ManagerFK] is not null
					</cfif>
					
				 ) as sub				  
				 
			 group by JobNumber, JobID, GMVID, jobName, jobNumber, Number
			 having SUM(cast(IsActionCompleted as int)) <> COUNT(IsActionCompleted)		
		</cfquery>
		
		<cfset var newJobArray = arrayNew(1)>
		<cfset var startedJobArray = arrayNew(1)>
		<cfset var completedJobArray = arrayNew(1)>
		<cfloop query="qGet">
			<cfset var aJobRecord = structNew()>
			<cfset ajobrecord["jobID"] = qGet.jobID>
			<cfset ajobrecord["jobName"] = ReplaceNoCase(qGet.jobName, 'Work Order: ', '', 'all')>
			<cfset ajobrecord["jobNumber"] = qGet.jobNumber>

			<cfset var completedAction = 0>
			<cfset ajobrecord["completedAction"] = qGet.Completed>
			<cfset ajobrecord["totalAction"] = qGet.Total>
			<cfif qGet.Completed eq 0>
					<cfset ajobrecord["status"] = 'new'>
					<cfset ajobrecord["class"] = 'label-success'>
					<cfset arrayappend(newJobArray, ajobrecord)>
			<cfelseif  qGet.Completed neq qGet.Total>
					<cfset ajobrecord["status"] = 'started'>
					<cfset ajobrecord["class"] = 'label-warning'>
					<cfset arrayappend(startedJobArray, ajobrecord)>
			<cfelse>
					<cfset ajobrecord["status"] = 'completed'>
					<cfset ajobrecord["class"] = 'label-info'>
					<cfset arrayappend(completedJobArray, ajobrecord)>
			</cfif>
		</cfloop>

		<cfset var utils = new cfc.Utils()>
		<cfset returnVal["workOrderJobs"] = utils.arrayAppendAll(returnVal["workOrderJobs"], startedJobArray)>
		<cfset returnVal["workOrderJobs"] = utils.arrayAppendAll(returnVal["workOrderJobs"], newJobArray)>
		<cfset returnVal["workOrderJobs"] = utils.arrayAppendAll(returnVal["workOrderJobs"], completedJobArray)>

		<cfreturn returnVal />
	</cffunction>
</cfcomponent>
