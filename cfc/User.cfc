<cfcomponent extends="cfc.Global">
	<cffunction name="Login" access="remote" output="false" returnType="struct" returnformat="JSON">
		<cfargument name="username" type="string" required="true">
        <cfargument name="password" type="string" required="true">
        <!---Get the datasoure first--->

		<cfset returnVal = structNew()>
		<cfset returnVal["success"] = false>
		<cfset returnVal["message"] = ''>

		<cfset var datasource = getdatasource(userName = userName)>
		<cfif len(datasource) eq 0>
			<cfset returnVal["success"] = false>
			<cfset returnVal["message"] = 'Please ensure you have added the correct suffix to your username.'>
			<cfreturn returnVal>
		</cfif>

		<cfset var useEmail = "">
		<cfset var tempArray = listToArray(arguments.username, '.')>
		<cfif arraylen(tempArray) gt 1>
<!---should have at least two elements to make sense, i.e. debug@coms.net.nz.gi  --->
			<cfset var prefix = tempArray[arraylen(tempArray)]> <!---Get the last Element --->
			<cfset useEmail = left(arguments.username, len(arguments.username) - len(prefix) -1)>
		</cfif>

		<cfset var qSet = "">
		<cfquery name="qGet" datasource="#datasource#">
			SELECT *
			  FROM [userData]
			  where logonUPN = <cfqueryparam cfsqltype="cf_sql_varchar" value="#useEmail#">
			  AND logonPassword = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.password#">
		</cfquery >

		<cfif qGet.recordcount eq 1>
			<cfset returnVal["success"] = true>
			<cfset returnVal["message"] = ''>
			<cfset session.userID = qGet.userID>
			<cfset session.fullName = qGet.fullName>
			<cfset session.logonUPN = qGet.logonUPN>
			<cfset session.datasource = datasource>
		<cfelse>
			<cfset returnVal["message"] = 'Email or password is incorrect.'>
		</cfif>
		<cfreturn returnVal>
	</cffunction>


	<cffunction name="Logout" access="remote" output="false" returnType="struct" returnformat="JSON">
		<cfset returnVal = structNew()>
		<cfset returnVal["success"] = true>
		<cfset returnVal["message"] = ''>

		<!---clear session --->
		<cfset structClear(session)>

		<cfreturn returnVal>
	</cffunction>

	<cffunction name="isAuthorized" access="remote" output="false" returnType="struct" returnformat="JSON">
		<cfset returnVal = structNew()>
		<cfset returnVal["success"] = false>
		<cfset returnVal["message"] = ''>
		<cfreturn returnVal>
	</cffunction>


</cfcomponent>
