<cfcomponent extends="global">

    <cffunction name="getAllByUser" access="remote" returnType="struct" output="false" returnformat="JSON">
        <cfargument name="isFromWorkOrder" type="string" required="false" default="1">
        <cfset var qGet = ''>
        <cfquery name="qGet" datasource="#session.datasource#">
            SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
            SELECT top 1000 smeJobs.jobID, smeJobs.jobName, smeJobs.jobCreatedBy, smeJobs.jobStatus, smeJobs.jobPriority, smeJobs.jobSLA, smeJobs.jobSLATimer,
								  smeJobs.jobType, smeJobs.jobDescription, smeJobs.jobCreationDate, smeJobs.jobLastUpdated, smeJobs.jobACCNO, smeJobs.jobSEQNO, smeJobs.jobNumber,
								  smeJobs.jobEmailTo, smeJobs.jobEmailFrom, smeJobs.jobAssignedTo, smeJobs.jobScheduledDate, smeJobs.jobArrivedOnSite, smeJobs.jobCompleteStamp, smeJobs.timeToOnSite, smeJobs.timeToCompleted, smeJobs.jobEffort,
								  smeJobs.jobReference, ClientAccounts.NAME, smeJobs.jobMessageID, smeJobs.jobRecentUpdate, smeJobs.jobTimeStamp, smeJobs.jobFlag, smeJobs.exchangeUID_task, smeJobs.exchangeUID_calendar, smeJobs.isFromWorkOrder
				FROM smeJobs INNER JOIN ClientAccounts ON smeJobs.jobACCNO = ClientAccounts.ACCNO

				WHERE (
					    smeJobs.jobStatus NOT IN (
								 SELECT [jobStatusID]
									  FROM [lib_jobStatus]
										WHERE [jobStatusName] in (
										'complete'
										,'Closed'
										,'Cancelled'
									  )

						 )
				    )
				AND	jobDeleted = 0                
			    AND isFromWorkOrder = 1
                AND smeJobs.jobID in (
                      	SELECT [JobFK]
                          FROM [lib_jobAssignedUsers]
                          INNER JOIN [assignmentUsers] on [assignmentUsers].auID = [lib_jobAssignedUsers].assignmentUserFK
                          INNER JOIN [lib_User_AssignmentUser] on [lib_User_AssignmentUser].[AUFK] = [lib_jobAssignedUsers].assignmentUserFK
                          WHERE [lib_User_AssignmentUser].[UserFK] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.userID#">
                )
                AND JobID in (
                    SELECT JOBID FROM [workOrder_GreatPlain]
                    WHERE [WorkOrderID] in (
                        SELECT [ID]
                        FROM [workOrder]
                        WHERE [workOrder].ID in (
                                SELECT [WorkOrderFK]
                            FROM [workOrderSub_Machine]
                            WHERE ID in (
                                SELECT [MachineFK]
                                FROM [workOrderSub_MachineItem]
                            )
                        )
                    )
                )                
                ORDER BY jobCreationDate DESC
        </cfquery>

        <cfset var returnVal = structNew()>
        <cfset returnVal["success"] = true>
        <cfset returnVal["message"] = ''>
        <cfset returnVal["workOrderJobs"] = arrayNew(1)>

        <cfset var newJobArray = arrayNew(1)>
        <cfset var startedJobArray = arrayNew(1)>
        <cfset var completedJobArray = arrayNew(1)>

        <cfloop query="qGet">
            <cfset var qJobs = getAllByJobID(qGet.jobID)>

            <cfif isQuery(qJobs) and qJobs.recordCount gt 0>

                <!---there is an machine job--->
                <cfset var aJobRecord = structNew()>
                <cfset ajobrecord["jobID"] = qGet.jobID>
                <cfset ajobrecord["jobName"] = ReplaceNoCase(qGet.jobName, 'Work Order: ', '', 'all')>
                <cfset ajobrecord["jobNumber"] = qGet.jobNumber>

                <cfset var totalAction = qJobs.recordcount>
                <cfset var completedAction = 0>
                <cfset var lastActionCompletedDate = now()>
                <cfloop query="qJobs">
                    <cfif IsActionCompleted eq 1>
                        <cfset completedAction = completedAction +1>
                        <cfif DateCompare(qJobs.ActionCompletedOn, lastActionCompletedDate) eq -1> <!---ActionCompletedOn earlier than lastActionCompletedDate--->
                            <cfset lastActionCompletedDate =  qJobs.ActionCompletedOn>
                        </cfif>
                    </cfif>
                </cfloop>

                <cfif completedAction neq 0 and totalAction eq completedAction>
                    <!--- all completed, check the last complete date.  --->
                    <cfif DateDiff("d", lastActionCompletedDate, now()) gt 3 or DateDiff("d", lastActionCompletedDate, now()) lt -3>
                        <cfcontinue>
                    </cfif>
                </cfif>

                <cfset ajobrecord["completedAction"] = completedAction>
                <cfset ajobrecord["totalAction"] = totalAction>

                <cfif completedAction eq 0>
                    <cfset ajobrecord["status"] = 'new'>
                    <cfset ajobrecord["class"] = 'label-success'>
                    <cfset arrayappend(newJobArray, ajobrecord)>
                <cfelseif  completedAction neq totalAction>
                    <cfset ajobrecord["status"] = 'started'>
                    <cfset ajobrecord["class"] = 'label-warning'>
                    <cfset arrayappend(startedJobArray, ajobrecord)>
                <cfelse>
                    <cfset ajobrecord["status"] = 'completed'>
                    <cfset ajobrecord["class"] = 'label-info'>
                    <cfset arrayappend(completedJobArray, ajobrecord)>
                </cfif>
            </cfif>
        </cfloop>

        <cfset var utils = new cfc.Utils()>
        <cfset returnVal["workOrderJobs"] = utils.arrayAppendAll(returnVal["workOrderJobs"], startedJobArray)>
        <cfset returnVal["workOrderJobs"] = utils.arrayAppendAll(returnVal["workOrderJobs"], newJobArray)>
        <cfset returnVal["workOrderJobs"] = utils.arrayAppendAll(returnVal["workOrderJobs"], completedJobArray)>

        <cfreturn returnVal />
    </cffunction>
     <cffunction name="getAllByJobID" access="public" returnType="any" output="false">
        <cfargument name="jobID" type="string" required="true">

        <cfset var qSupplier = ''>
        <cfquery name="qSupplier" datasource="#session.datasource#">
            SELECT TOP 1 [WorkOrderID]
                  ,[JobID]
                  ,[SupplierName]
                  ,[SupplierID]
              FROM [workOrder_GreatPlain]
              where JobID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.jobID#">
        </cfquery>
        <cfset var qGet = QueryNew("")>
        <cfif len(qSupplier.SupplierID) eq 36>
            <!---Only get this suppliers Job--->
            <cfquery name="qGet" datasource="#session.datasource#">
                    SELECT TOP 1000 [workOrderSub_MachineItem].[ID]
                        ,[workOrderSub_Machine].WorkOrderFK AS workorderID
                        ,workOrderSub_MachineItem.MachineFK AS workorderMachineTabID
                        ,workOrderSub_MachineItem.ID AS workorderMachineItemID
                        ,[workOrder_lib_machine_Action].actionName
                        ,[workOrderSub_MachineItem].JIN
                        ,[workOrderSub_MachineItem].serialNumber
                        ,[ClientAccounts].NAME as venueName
                        ,[workOrder].Number as workorderNumber

                        ,[IsActionCompleted]
                        ,[ActionCompletedBy]
                        ,[ActionCompletedOn]

                  FROM [workOrderSub_MachineItem]
                  INNER JOIN [workOrderSub_Machine] on [workOrderSub_Machine].ID = [workOrderSub_MachineItem].MachineFK
                  INNER JOIN [workOrder] on [workOrder].ID =   [workOrderSub_Machine].WorkOrderFK
                  INNER JOIN [workOrder_lib_machine_Action] on [workOrderSub_MachineItem].ActionFK = workOrder_lib_machine_Action.ID
                  INNER JOIN [ClientAccounts] on workOrder.accnoFK = ClientAccounts.ACCNO

                  Where workOrder.id = <cfqueryparam cfsqltype="cf_sql_varchar" value="#qSupplier.WorkOrderID#">                  

                ORDER BY workOrderSub_MachineItem.RecordCreatedon
            </cfquery>
        <cfelseif qSupplier.recordcount eq 1>
            <!---Only get NON - suppliers Job--->
            <cfquery name="qGet" datasource="#session.datasource#">
                    SELECT TOP 1000 [workOrderSub_MachineItem].[ID]
                        ,[workOrderSub_Machine].WorkOrderFK AS workorderID
                        ,workOrderSub_MachineItem.MachineFK AS workorderMachineTabID
                        ,workOrderSub_MachineItem.ID AS workorderMachineItemID
                        ,[workOrder_lib_machine_Action].actionName
                        ,[workOrderSub_MachineItem].JIN
                        ,[workOrderSub_MachineItem].serialNumber
                        ,[ClientAccounts].NAME as venueName
                        ,[workOrder].Number as workorderNumber

                        ,[IsActionCompleted]
                        ,[ActionCompletedBy]
                        ,[ActionCompletedOn]


                  FROM [workOrderSub_MachineItem]
                  INNER JOIN [workOrderSub_Machine] on [workOrderSub_Machine].ID = [workOrderSub_MachineItem].MachineFK
                  INNER JOIN [workOrder] on [workOrder].ID =   [workOrderSub_Machine].WorkOrderFK
                  INNER JOIN [workOrder_lib_machine_Action] on [workOrderSub_MachineItem].ActionFK = workOrder_lib_machine_Action.ID
                  INNER JOIN [ClientAccounts] on workOrder.accnoFK = ClientAccounts.ACCNO

                  Where workOrder.id = <cfqueryparam cfsqltype="cf_sql_varchar" value="#qSupplier.WorkOrderID#">
                AND WarehouseFK = NULL

                ORDER BY workOrderSub_MachineItem.RecordCreatedon
            </cfquery>
        </cfif>

        <cfreturn qGet />
    </cffunction>

    <cffunction name="getAllJobsForUserList" access="remote" returnType="struct" output="true" returnformat="JSON" hint="Given a list of venues in string format, loop through venues and get all work order jobs">
        <cfargument name="venueListString" type="string" required="true">
        <cfargument name="isFromWorkOrder" type="string" required="false" default="1">

        <cftry>
            <cfset venueList = listToArray(arguments.venueListString)>

            <cfset var returnVal = structNew()>
            <cfset returnVal["success"] = true>
            <cfset returnVal["message"] = ''>
            <cfset returnVal["jobsForUser"] = arrayNew(1)>

            <cfset var utils = new cfc.Utils()>

            <cfloop index="currentAccno" array="#venueList#">
                <cfset var venueJobArray = getAllByVenue(currentAccno)>
                <cfset returnVal["jobsForUser"] = utils.arrayAppendAll(returnVal["jobsForUser"], venueJobArray.workOrderJobs)>
            </cfloop>

            <cfset var userOtherJobArray = getUserJobsOtherThanWorkorderJobs()>      
            <!--- setting the workOrderJobs object because this object is used throughout the application --->      
            <cfset returnVal["jobsForUser"] = utils.arrayAppendAll(returnVal["jobsForUser"], userOtherJobArray.workOrderJobs)> 

            <cfreturn returnVal>

            <cfcatch type="any">
                <cfset returnVal["success"] = false>
                <cfset returnVal["message"] = ''>

                <cfreturn returnVal>
            </cfcatch>
        </cftry>
    </cffunction>

    <cffunction name="getAllByVenue" access="remote" returnType="struct" output="false" returnformat="JSON">
        <cfargument name="venueAccno" type="numeric" required="true">
        <cfargument name="isFromWorkOrder" type="string" required="false" default="1">
        <cfset var qGet = ''>
        <cfquery name="qGet" datasource="#session.datasource#">
            SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
            SELECT top 1000 smeJobs.jobID, smeJobs.jobName, smeJobs.jobCreatedBy, smeJobs.jobStatus, smeJobs.jobPriority, smeJobs.jobSLA, smeJobs.jobSLATimer,
								  smeJobs.jobType, smeJobs.jobDescription, smeJobs.jobCreationDate, smeJobs.jobLastUpdated, smeJobs.jobACCNO, smeJobs.jobSEQNO, smeJobs.jobNumber,
								  smeJobs.jobEmailTo, smeJobs.jobEmailFrom, smeJobs.jobAssignedTo, smeJobs.jobScheduledDate, smeJobs.jobArrivedOnSite, smeJobs.jobCompleteStamp, smeJobs.timeToOnSite, smeJobs.timeToCompleted, smeJobs.jobEffort,
								  smeJobs.jobReference, ClientAccounts.NAME, smeJobs.jobMessageID, smeJobs.jobRecentUpdate, smeJobs.jobTimeStamp, smeJobs.jobFlag, smeJobs.exchangeUID_task, smeJobs.exchangeUID_calendar, smeJobs.isFromWorkOrder
				FROM smeJobs INNER JOIN ClientAccounts ON smeJobs.jobACCNO = ClientAccounts.ACCNO

				WHERE (
					    smeJobs.jobStatus NOT IN (
								 SELECT [jobStatusID]
									  FROM [lib_jobStatus]
										WHERE [jobStatusName] in (
										'complete'
										,'Closed'
										,'Cancelled'
									  )

						 )
				    )
				AND	jobDeleted = 0
				AND isFromWorkOrder = 1
				AND JobID in (
					SELECT JOBID FROM [workOrder_GreatPlain]
					WHERE [WorkOrderID] in (
						SELECT [ID]
						  FROM [workOrder]
						  WHERE [workOrder].ID in (
								SELECT [WorkOrderFK]
							  FROM [workOrderSub_Machine]
							  WHERE ID in (
								SELECT [MachineFK]
								  FROM [workOrderSub_MachineItem]
							  )
						  )
					)
				)
				AND smeJobs.jobAccno = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.venueAccno#">
                ORDER BY jobCreationDate DESC
        </cfquery>

        <cfset var returnVal = structNew()>
        <cfset returnVal["success"] = true>
        <cfset returnVal["message"] = ''>
        <cfset returnVal["workOrderJobs"] = arrayNew(1)>

        <cfset var newJobArray = arrayNew(1)>
        <cfset var startedJobArray = arrayNew(1)>
        <cfset var completedJobArray = arrayNew(1)>

        <cfloop query="qGet">
            <cfset var qJobs = getAllByJobID(qGet.jobID)>

            <cfif isQuery(qJobs) and qJobs.recordCount gt 0>

                <!---there is an machine job--->
                <cfset var aJobRecord = structNew()>
                <cfset ajobrecord["jobID"] = qGet.jobID>
                <cfset ajobrecord["jobName"] = ReplaceNoCase(qGet.jobName, 'Work Order: ', '', 'all')>
                <cfset ajobrecord["jobNumber"] = qGet.jobNumber>

                <cfset var totalAction = qJobs.recordcount>
                <cfset var completedAction = 0>
                <cfset var lastActionCompletedDate = now()>
                <cfloop query="qJobs">
                    <cfif IsActionCompleted eq 1>
                        <cfset completedAction = completedAction +1>
                        <cfif DateCompare(qJobs.ActionCompletedOn, lastActionCompletedDate) eq -1> <!---ActionCompletedOn earlier than lastActionCompletedDate--->
                            <cfset lastActionCompletedDate =  qJobs.ActionCompletedOn>
                        </cfif>
                    </cfif>
                </cfloop>

                <cfif completedAction neq 0 and totalAction eq completedAction>
                    <!--- all completed, check the last complete date.  --->
                    <cfif DateDiff("d", lastActionCompletedDate, now()) gt 3 or DateDiff("d", lastActionCompletedDate, now()) lt -3>
                        <cfcontinue>
                    </cfif>
                </cfif>

                <cfset ajobrecord["completedAction"] = completedAction>
                <cfset ajobrecord["totalAction"] = totalAction>

                <cfif completedAction eq 0>
                    <cfset ajobrecord["status"] = 'new'>
                    <cfset ajobrecord["class"] = 'label-success'>
                    <cfset arrayappend(newJobArray, ajobrecord)>
                    <cfelseif  completedAction neq totalAction>
                    <cfset ajobrecord["status"] = 'started'>
                    <cfset ajobrecord["class"] = 'label-warning'>
                    <cfset arrayappend(startedJobArray, ajobrecord)>
                <cfelse>
                    <cfset ajobrecord["status"] = 'completed'>
                    <cfset ajobrecord["class"] = 'label-info'>
                    <cfset arrayappend(completedJobArray, ajobrecord)>
                </cfif>
            </cfif>
        </cfloop>

        <cfset var utils = new cfc.Utils()>
        <cfset returnVal["workOrderJobs"] = utils.arrayAppendAll(returnVal["workOrderJobs"], startedJobArray)>
        <cfset returnVal["workOrderJobs"] = utils.arrayAppendAll(returnVal["workOrderJobs"], newJobArray)>
        <cfset returnVal["workOrderJobs"] = utils.arrayAppendAll(returnVal["workOrderJobs"], completedJobArray)>

        <cfreturn returnVal />
    </cffunction>

    <cffunction name="Install" access="remote" output="false" returnType="struct" returnformat="JSON">
        <cfargument name="machineItemID" type="any" required="false">

        <cfset var returnVal = structNew()>
        <cfset returnVal["success"] = true>
        <cfset returnVal["message"] = ''>
        <cfset returnVal["IsActionCompleted"] = '0'>
        <cfset returnVal["machines"] = arrayNew(1)>
        <cfset var qGet = getInstallQuery(arguments.machineItemID)>

        <cfset var machineStruct = structNew()>
        <cfset machineStruct["name"] =  qGet.machineGameName>
        <cfset machineStruct["serial"] = qGet.machineSerialNumber>
        <cfset machineStruct["target"] = qGet.JIN>
        <cfset machineStruct["IsActionCompleted"] = qGet.IsActionCompleted>
        <cfset returnVal["IsActionCompleted"] = qGet.IsActionCompleted>
        <cfif qGet.IsActionCompleted eq 1>
            <cfset machineStruct["class"] = 'label-info'>
            <cfset machineStruct["status"] = 'completed'>
        <cfelse>
            <cfset machineStruct["class"] = 'label-success'>
            <cfset machineStruct["status"] = 'new'>
        </cfif>

        <cfset arrayAppend(returnVal["machines"], machineStruct)>

        <cfreturn returnVal>
    </cffunction>

    <cffunction name="remove" access="remote" output="false" returnType="struct" returnformat="JSON">
        <cfargument name="machineItemID" type="any" required="false">

        <cfset var returnVal = structNew()>
        <cfset returnVal["success"] = true>
        <cfset returnVal["message"] = ''>
        <cfset returnVal["machines"] = arrayNew(1)>

        <cfset var qGet = getRemoveQuery(arguments.machineItemID)>

        <cfset var machineStruct = structNew()>
        <cfset machineStruct["name"] =  qGet.machineGameName>
        <cfset machineStruct["serial"] = qGet.machineSerialNumber>
        <cfset machineStruct["jin"] = qGet.JIN>
        <cfset machineStruct["IsActionCompleted"] = qGet.IsActionCompleted>
        <cfset returnVal["IsActionCompleted"] = qGet.IsActionCompleted>
        <cfif qGet.IsActionCompleted eq 1>
            <cfset machineStruct["class"] = 'label-info'>
            <cfset machineStruct["status"] = 'completed'>
        <cfelse>
            <cfset machineStruct["class"] = 'label-success'>
            <cfset machineStruct["status"] = 'new'>
        </cfif>
        <cfset arrayAppend(returnVal["machines"], machineStruct)>
        <cfreturn returnVal>
    </cffunction>

    <cffunction name="relocate" access="remote" output="false" returnType="struct" returnformat="JSON">
        <cfargument name="machineItemID" type="any" required="false">

        <cfset var returnVal = structNew()>
        <cfset returnVal["success"] = true>
        <cfset returnVal["message"] = ''>
        <cfset returnVal["machines"] = arrayNew(1)>

        <cfset var qGet = getrelocatequery(arguments.machineItemID)>

        <cfset var machineStruct = structNew()>
        <cfset machineStruct["actionName"] =  qGet.actionName>
        <cfset machineStruct["name"] =  qGet.machineGameName>
        <cfset machineStruct["serial"] = qGet.machineSerialNumber>
        <cfset machineStruct["jin"] = qGet.JIN>
        <cfset machineStruct["IsActionCompleted"] = qGet.IsActionCompleted>
        <cfset returnVal["IsActionCompleted"] = qGet.IsActionCompleted>
        <cfif qGet.IsActionCompleted eq 1>
            <cfset machineStruct["class"] = 'label-info'>
            <cfset machineStruct["status"] = 'completed'>
        <cfelse>
            <cfset machineStruct["class"] = 'label-success'>
            <cfset machineStruct["status"] = 'new'>
        </cfif>
        <cfset arrayAppend(returnVal["machines"], machineStruct)>

        <cfreturn returnVal>
    </cffunction>

    <cffunction name="new" access="remote" output="false" returnType="struct" returnformat="JSON">
        <cfargument name="machineItemID" type="any" required="false">

        <cfset var returnVal = structNew()>
        <cfset returnVal["success"] = true>
        <cfset returnVal["message"] = ''>
        <cfset returnVal["machines"] = arrayNew(1)>

        <cfset var qGet = getnewquery(arguments.machineItemID)>

        <cfset var machineStruct = structNew()>
        <cfset machineStruct["name"] =  qGet.machineGameName>
        <cfset machineStruct["serial"] = qGet.machineSerialNumber>
        <cfset machineStruct["jin"] = qGet.JIN>
        <cfset machineStruct["IsActionCompleted"] = qGet.IsActionCompleted>
        <cfset returnVal["IsActionCompleted"] = qGet.IsActionCompleted>
        <cfif qGet.IsActionCompleted eq 1>
            <cfset machineStruct["class"] = 'label-info'>
            <cfset machineStruct["status"] = 'completed'>
        <cfelse>
            <cfset machineStruct["class"] = 'label-success'>
            <cfset machineStruct["status"] = 'new'>
        </cfif>

        <cfset arrayAppend(returnVal["machines"], machineStruct)>

        <cfreturn returnVal>
    </cffunction>

    <cffunction name="conversion" access="remote" output="false" returnType="struct" returnformat="JSON">
        <cfargument name="machineItemID" type="any" required="false">

        <cfset var returnVal = structNew()>
        <cfset returnVal["success"] = true>
        <cfset returnVal["message"] = ''>
        <cfset returnVal["machines"] = arrayNew(1)>

        <cfset var qGet = getConversionQuery(arguments.machineItemID)>
        <cfset var machineStruct = structNew()>
        <cfset machineStruct["existing_name"] =  qGet.ExistingGameName>
        <cfset machineStruct["existing_serial"] = qGet.SerialNumber>
        <cfset machineStruct["existing_jin"] = qGet.JIN>
        <cfset machineStruct["proposed_gameName"] =  qGet.gameName>
        <cfset machineStruct["proposed_gameApprovalNumber"] = qGet.gameApprovalNumber>
        <cfset machineStruct["IsActionCompleted"] = qGet.IsActionCompleted>
        <cfset returnVal["IsActionCompleted"] = qGet.IsActionCompleted>
        <cfif qGet.IsActionCompleted eq 1>
            <cfset machineStruct["class"] = 'label-info'>
            <cfset machineStruct["status"] = 'completed'>
        <cfelse>
            <cfset machineStruct["class"] = 'label-success'>
            <cfset machineStruct["status"] = 'new'>
        </cfif>

        <cfset arrayAppend(returnVal["machines"], machineStruct)>

        <cfreturn returnVal>
    </cffunction>

    <cffunction name="variationChange" access="remote" output="false" returnType="struct" returnformat="JSON">
        <cfargument name="machineItemID" type="any" required="false">

        <cfset var returnVal = structNew()>
        <cfset returnVal["success"] = true>
        <cfset returnVal["message"] = ''>
        <cfset returnVal["machines"] = arrayNew(1)>

        <cfquery name="qGet" datasource="#session.datasource#">
             SELECT TOP 1000 [ID]
                  ,[MachineFK]
                  ,[JIN]
                  ,[SerialNumber] AS machineSerialNumber
                  ,[ExistingGameName] AS machineGameName
                  ,[WarehouseFK]
                  ,[ManufacturerFK]
                  ,[GANFK]
                  ,[ProposedGameFK]
                  ,[ActionFK]
                  ,[Cost]
                  ,[RecordCreatedon]
                  ,[RecordCreatedBy]
                  ,[woSub_PO_number]

                  ,[IsActionCompleted]
                  ,[ActionCompletedBy]
                  ,[ActionCompletedOn]

              FROM [workOrderSub_MachineItem]
              Where ID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.machineItemID#">
        </cfquery>

        <cfset var machineStruct = structNew()>
        <cfset machineStruct["name"] =  qGet.machineGameName>
        <cfset machineStruct["serial"] = qGet.machineSerialNumber>
        <cfset machineStruct["jin"] = qGet.JIN>
        <cfset machineStruct["IsActionCompleted"] = qGet.IsActionCompleted>
        <cfset returnVal["IsActionCompleted"] = qGet.IsActionCompleted>
        <cfif qGet.IsActionCompleted eq 1>
            <cfset machineStruct["class"] = 'label-info'>
            <cfset machineStruct["status"] = 'completed'>
        <cfelse>
            <cfset machineStruct["class"] = 'label-success'>
            <cfset machineStruct["status"] = 'new'>
        </cfif>

        <cfset arrayAppend(returnVal["machines"], machineStruct)>

        <cfreturn returnVal>
    </cffunction>

    <cffunction name="confirmation" access="remote" output="false" returnType="struct" returnformat="JSON">
        <cfargument name="machineItemID" type="any" required="true">
        <cfargument name="workOrderID" type="any" required="true">

        <cfset var returnVal = structNew()>
        <cfset returnVal["success"] = true>
        <cfset returnVal["message"] = ''>
        <cfset returnVal["machines"] = arrayNew(1)>

        <!---first, we need to know the action --->
        <cfset var qAction = "">
        <cfquery name="qAction" datasource="#session.datasource#">
            SELECT [actionName]
              FROM [workOrder_lib_machine_Action]
              INNER JOIN [workOrderSub_MachineItem] on [workOrderSub_MachineItem].ActionFK  =  [workOrder_lib_machine_Action].ID
              WHERE [workOrderSub_MachineItem].ID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.machineItemID#">
        </cfquery>

        <cfif qAction.recordcount neq 1>
            <!---error--->
            <cfset returnVal["success"] = false>
            <cfset returnVal["message"] = 'The machine action is not defined.'>
            <cfreturn returnVal>
        </cfif>
        <!---<cfset var actionDetail = getactiondetail(qAction.actionName)>--->

        <!---get all the current machines --->
        <cfset var qMachines = "">
        <cfquery name="qMachines" datasource="#session.datasource#">
            SELECT TOP 1000 [machineID]
                      ,[accno]
                      ,[machineJINNumber]
                      ,[machineGameName]
                      ,[machineSerialNumber]
                      ,[machineGUID]
                  FROM [custom_Machines]
                  INNER JOIN [workOrder] on [workOrder].accnoFK =  [custom_Machines].accno
                  Where [workOrder].ID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.workOrderID#">
                ORDER BY [machineJINNumber]
        </cfquery>

        <cfset var isRelocation = false>
        <cfset var relocateTo = 0>
        <cfset var relocateFrom = 0>
        <cfset var relocateMachineGameName = ''>
        <cfset var relocateMachineSerialNumber = ''>

        <cfif left(qAction.ActionName, 5) eq 'Reloc'>
            <cfset isRelocation = true>
            <cfset var tempActionArray = ListToArray(qAction.ActionName, ' JIN ', false, true)>

            <cfif arraylen(tempActionArray) neq 2 OR  not isNumeric(tempActionArray[2])>
                <cfset returnVal["success"] = false>
                <cfset returnVal["message"] = 'The machine action is not defined.'>
                <cfreturn returnVal>
            </cfif>
            <cfset relocateTo = tempActionArray[2]>

            <cfset var qMachineItem = getRelocateQuery(arguments.machineItemID)>
            <cfset relocateFrom = qMachineItem.JIN>

        </cfif>

        <!---for each machine--->
        <cfloop query="qMachines">
            <cfset var machineStruct = structNew()>
<!---for different action, we need to handle it differently --->

            <cfswitch expression="#left(qAction.ActionName, 5)#">
                <cfcase value="Conve">
                    <!---Conversion & Conversion - WARRANTY--->
                    <cfset var qMachineItem = getconversionquery(arguments.machineItemID)>
                    <cfif qMachines.machineSerialNumber eq qmachineitem.SerialNumber>
                        <!---the machine we are dealing with --->
                        <cfset machineStruct["name"] =  qMachineItem.gameName> <!---will be the new game ?--->
                        <cfset machineStruct["serial"] = machineSerialNumber> <!------>
                        <cfset machineStruct["jin"] = machineJINNumber>
                        <cfset machineStruct["class"] = 'danger'>
                        <cfset arrayAppend(returnVal["machines"], machineStruct)>
                    <cfelse>
                        <!---Normal machines--->
                        <cfset machineStruct["name"] =  machineGameName>
                        <cfset machineStruct["serial"] = machineSerialNumber>
                        <cfset machineStruct["jin"] = machineJINNumber>
                        <cfset machineStruct["class"] = ''>
                        <cfset arrayAppend(returnVal["machines"], machineStruct)>
                    </cfif>
                </cfcase>

                <cfcase value="Insta">
                    <!---Install--->
                    <cfset var qMachineItem = getInstallQuery(arguments.machineItemID)>
                    <cfif qMachineItem.JIN eq qMachines.machineJINNumber>
                        <!---Will install onto this JIN--->
                        <cfset machineStruct["name"] =  qMachineItem.machineGameName>
                        <cfset machineStruct["serial"] = qMachineItem.machineSerialNumber>
                        <cfset machineStruct["jin"] = machineJINNumber>
                        <cfset machineStruct["class"] = 'danger'>
                        <cfset arrayAppend(returnVal["machines"], machineStruct)>
                    <cfelse>
                        <cfset machineStruct["name"] =  machineGameName>
                        <cfset machineStruct["serial"] = machineSerialNumber>
                        <cfset machineStruct["jin"] = machineJINNumber>
                        <cfset machineStruct["class"] = ''>
                        <cfset arrayAppend(returnVal["machines"], machineStruct)>
                    </cfif>
                </cfcase>

                <cfcase value="New">
                    <!---New--->
                    <cfset var qMachineItem = getNewQuery(arguments.machineItemID)>
                    <cfset machineStruct["name"] =  machineGameName>
                    <cfset machineStruct["serial"] = machineSerialNumber>
                    <cfset machineStruct["jin"] = machineJINNumber>
                    <cfset machineStruct["class"] = ''>
                    <cfset arrayAppend(returnVal["machines"], machineStruct)>
                    <!---add one more item to the end of list --->
                    <cfif qMachines.recordcount eq qMachines.currentrow>
                        <cfset var machineStruct = structNew()>
                        <cfset var qMachineItem = getNewQuery(arguments.machineItemID)>
                        <cfset machineStruct["name"] =  qMachineItem.machineGameName>
                        <cfset machineStruct["serial"] = qMachineItem.machineSerialNumber>
                        <cfset machineStruct["jin"] = '99'>
                        <cfset machineStruct["class"] = 'danger'>
                        <cfset arrayAppend(returnVal["machines"], machineStruct)>
                    </cfif>
                </cfcase>

                <cfcase value="Reloc">
                    <!---Relocation--->

                    <!---need to know where the machine relocated to --->


                    <cfif machineJINNumber eq relocateFrom>
                        <!---we should leave the FROM jin blank here, but keep the machine --->
                        <cfset relocateMachineGameName = machineGameName>
                        <cfset relocateMachineSerialNumber = machineSerialNumber>
                        <cfset machineStruct["name"] =  "">
                        <cfset machineStruct["serial"] = "">
                        <cfset machineStruct["jin"] = machineJINNumber>
                        <cfset machineStruct["class"] = 'danger'>
                        <cfset arrayAppend(returnVal["machines"], machineStruct)>
                    <cfelseif machineJINNumber eq relocateTo>
                        <!---we should put the new machine here, but we will need to update late as we dont' know the the game name yet--->
                        <cfset machineStruct["name"] =  "">
                        <cfset machineStruct["serial"] = "">
                        <cfset machineStruct["jin"] = relocateTo>
                        <cfset machineStruct["class"] = 'success'>
                        <cfset arrayAppend(returnVal["machines"], machineStruct)>
                    <cfelse>
                        <cfset machineStruct["name"] =  machineGameName>
                        <cfset machineStruct["serial"] = machineSerialNumber>
                        <cfset machineStruct["jin"] = machineJINNumber>
                        <cfset machineStruct["class"] = ''>
                        <cfset arrayAppend(returnVal["machines"], machineStruct)>
                    </cfif>

                </cfcase>

                <cfcase value="Remov">
                    <!---Removal--->
                    <cfset var qMachineItem = getremovequery(arguments.machineItemID)>
                    <cfif qmachineitem.machineSerialNumber eq machineSerialNumber>
                        <cfset machineStruct["name"] =  ''>
                        <cfset machineStruct["serial"] = ''>
                        <cfset machineStruct["jin"] = machineJINNumber>
                        <cfset machineStruct["class"] = 'danger'>
                        <cfset arrayAppend(returnVal["machines"], machineStruct)>
                    <cfelse>
                        <cfset machineStruct["name"] =  machineGameName>
                        <cfset machineStruct["serial"] = machineSerialNumber>
                        <cfset machineStruct["jin"] = machineJINNumber>
                        <cfset machineStruct["class"] = ''>
                        <cfset arrayAppend(returnVal["machines"], machineStruct)>
                    </cfif>
                </cfcase>

                <cfcase value="Varia">
                    <!---Variation Change--->
                    <cfset machineStruct["name"] =  machineGameName>
                    <cfset machineStruct["serial"] = machineSerialNumber>
                    <cfset machineStruct["jin"] = machineJINNumber>
                    <cfset machineStruct["class"] = ''>
                    <cfset arrayAppend(returnVal["machines"], machineStruct)>
                </cfcase>
                <cfdefaultcase>
                    <cfmail from="debug@layerx.co.nz" to="fanhua@layerx.co.nz" subject="Unknow Action Type" type="HTML">
                        COMS Mobile - cfc/Workorder.cfc method: [confirmation]
                        <br/>
                        <cfdump var="#qAction#">
                    </cfmail>
                </cfdefaultcase>
            </cfswitch>
        </cfloop>
        <!---Update the relocateTo machine here is it is relocation --->
        <cfif isRelocation eq true>
            <cfset returnVal["machines"][relocateTo]["name"] = relocateMachineGameName>
            <cfset returnVal["machines"][relocateTo]["serial"] = relocateMachineSerialNumber>
        </cfif>

        <cfreturn returnVal>
    </cffunction>

    <cffunction name="performAction" access="remote" output="false" returnType="struct" returnformat="JSON">
        <cfargument name="machineItemID" type="any" required="true">
        <cfargument name="workOrderID" type="any" required="true">

        <cftry>


            <cfset var oEGM = new cfc.EGM()>

            <cfset var returnVal = structNew()>
            <cfset returnVal["success"] = true>
            <cfset returnVal["message"] = ''>
            <cfset returnVal["machines"] = arrayNew(1)>
            <!---first, we need to know the action --->
            <cfset var qAction = "">
            <cfquery name="qAction" datasource="#session.datasource#">
                SELECT [actionName]
                  FROM [workOrder_lib_machine_Action]
                  INNER JOIN [workOrderSub_MachineItem] on [workOrderSub_MachineItem].ActionFK  =  [workOrder_lib_machine_Action].ID
                  WHERE [workOrderSub_MachineItem].ID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.machineItemID#">
            </cfquery>

            <cfif qAction.recordcount neq 1>
                <!---error--->
                <cfset returnVal["success"] = false>
                <cfset returnVal["message"] = 'The machine action is not defined.'>
                <cfreturn returnVal>
            </cfif>

            <!---for each machine--->
                <cfset var machineStruct = structNew()>
                <cfset var actionValue = #left(qAction.ActionName, 5)#>
                <!---for different action, we need to handle it differently --->
                <cfswitch expression="#actionValue#">
                    <cfcase value="Conve">
                        <!---Conversion & Conversion - WARRANTY--->
                        <cfset var qMachineItem = getconversionquery(arguments.machineItemID)>
                        <cfset returnVal["success"] = oEGM.updateMachineGameName(machineSerialNumber =  qmachineitem.SerialNumber,  newGameName = qmachineitem.gameName)>
                    </cfcase>

                    <cfcase value="Insta">
                        <!---Install--->
                        <cfset var qMachineItem = getInstallQuery(arguments.machineItemID)>
                        <cfset returnVal["success"] = oEGM.updateMachineAccno(qMachineItem.machineSerialNumber, qMachineItem.venueAccno, false)>
                        <cfset returnVal["success"] = oEGM.updateMachineJin(qMachineItem.machineSerialNumber, qMachineItem.JIN)>
                    </cfcase>

                    <cfcase value="New">
                        <!---New--->
                        <cfset var qMachineItem = getNewQuery(arguments.machineItemID)>
                        <cfset returnVal["success"] = oEGM.updateMachineAccno(qMachineItem.machineSerialNumber, qMachineItem.venueAccno, false)>
                        <cfset returnVal["success"] = oEGM.updateMachineJin(qMachineItem.machineSerialNumber, '99')>
                    </cfcase>

                    <cfcase value="Reloc">
                        <!---Relocation--->
                        <cfset var tempActionArray = ListToArray(qAction.ActionName, ' JIN ', false, true)>
                        <cfif arraylen(tempActionArray) neq 2 OR  not isNumeric(tempActionArray[2])>
                            <cfset returnVal["success"] = false>
                            <cfset returnVal["message"] = 'The machine action is not defined.'>
                        </cfif>
                        <cfset var relocateTo = tempActionArray[2]>
                        <cfset var qMachineItem = getRelocateQuery(arguments.machineItemID)>
                        <cfset returnVal["success"] = oEGM.updateMachineJin(qMachineItem.machineSerialNumber, relocateTo,actionValue)>
                    </cfcase>

                    <cfcase value="Remov">
                        <!---Removal--->
                        <cfset var qMachineItem = getremovequery(arguments.machineItemID)>
                        <cfset returnVal["success"] = oEGM.updateMachineAccno(qmachineitem.machineSerialNumber, qmachineitem.WarehouseACCNO )>
                    </cfcase>
                    <cfcase value="Varia">
                        <!---Variation Change--->
                    </cfcase>
                    <cfdefaultcase>
                        <cfmail from="debug@layerx.co.nz" to="fanhua@layerx.co.nz" subject="Unknow Action Type" type="HTML">
                            COMS Mobile - cfc/Workorder.cfc method: [confirmation]
                            <br/>
                            <cfdump var="#qAction#">
                        </cfmail>
                    </cfdefaultcase>
                </cfswitch>

            <!---Update machine Item--->
            <cfset logMe('Update', 'Update Machine Item action status to complete [machineItemID: #arguments.machineItemID#]')>
            <cfset updateActionStatus(arguments.machineItemID)>
            <cfset emailVenue(arguments.workOrderID)>
            <cfcatch type="any">
                <cfmail from="debug@layerx.co.nz" to="debug@layerx.co.nz" subject="COMS Mobile Error" type="HTML">
                    <cfdump var="#cfcatch#">
                </cfmail>
            </cfcatch>
        </cftry>
        <cfreturn returnVal>
    </cffunction>

    <cffunction name="getConversionQuery" access="private" output="false" returnType="query">
        <cfargument name="machineItemID" type="any" required="true">
        <cfset var qGet = "">
        <cfquery name="qGet" datasource="#session.datasource#">
             SELECT TOP 1000 [ID]
                  ,[MachineFK]
                  ,[JIN]
                  ,[SerialNumber]
                  ,[ExistingGameName]
                  ,[WarehouseFK]
                  ,[ManufacturerFK]
                  ,[GANFK]
                  ,[ProposedGameFK]
                  ,[ActionFK]
                  ,[Cost]
                  ,[RecordCreatedon]
                  ,[RecordCreatedBy]
                  ,[woSub_PO_number]
                  ,[lib_machineGame].[gameName]
                  ,[lib_machineGame].[gameApprovalNumber]
                  ,[IsActionCompleted]
                  ,[ActionCompletedBy]
                  ,[ActionCompletedOn]
              FROM [workOrderSub_MachineItem]
            left outer JOIN [lib_machineGame] on [lib_machineGame].[gameID] =  [workOrderSub_MachineItem].[ProposedGameFK]
            where [workOrderSub_MachineItem].ID =  <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.machineItemID#">
        </cfquery>
        <cfreturn qGet>
    </cffunction>

    <cffunction name="getNewQuery" access="private" output="false" returnType="query">
        <cfargument name="machineItemID" type="any" required="true">
        <cfset var qGet = "">
        <cfquery name="qGet" datasource="#session.datasource#">
           SELECT TOP 1000 [workOrderSub_MachineItem].[ID]
                  ,[MachineFK]
                  ,[JIN]
                  ,[custom_Machines].machineSerialNumber
                  ,[custom_Machines].machineGameName

                ,[IsActionCompleted]
                ,[ActionCompletedBy]
                ,[ActionCompletedOn]
				,[workOrder].accnoFK AS venueAccno
              FROM [workOrderSub_MachineItem]
               INNER JOIN [workOrderSub_Machine] on [workOrderSub_Machine].ID = [workOrderSub_MachineItem].MachineFK
			  INNER JOIN [workOrder] on [workOrder].ID = [workOrderSub_Machine].WorkOrderFK
              left outer JOIN [custom_Machines] on [custom_Machines].machineGUID =  [workOrderSub_MachineItem].[ProposedGameFK]
              where [workOrderSub_MachineItem].ID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.machineItemID#">
        </cfquery>
        <cfreturn qGet>
    </cffunction>

    <cffunction name="getRelocateQuery" access="private" output="false" returnType="query">
        <cfargument name="machineItemID" type="any" required="true">
        <cfset var qGet = "">
        <cfquery name="qGet" datasource="#session.datasource#">
               SELECT TOP 1000 [workOrderSub_MachineItem].[ID]
                  ,[MachineFK]
                  ,[JIN]
                  ,[SerialNumber] AS machineSerialNumber
                  ,[ExistingGameName] AS machineGameName
                  ,[WarehouseFK]
                  ,[ManufacturerFK]
                  ,[GANFK]
                  ,[ProposedGameFK]
                  ,[ActionFK]
                  ,[Cost]
                  ,[RecordCreatedon]
                  ,[RecordCreatedBy]
                  ,[woSub_PO_number]
				  ,workOrder_lib_machine_Action.actionName
				  ,[IsActionCompleted]
                  ,[ActionCompletedBy]
                  ,[ActionCompletedOn]
              FROM [workOrderSub_MachineItem]
			  LEFT OUTER JOIN workOrder_lib_machine_Action on workOrder_lib_machine_Action.ID = [workOrderSub_MachineItem].ActionFK
              where [workOrderSub_MachineItem].ID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.machineItemID#">
        </cfquery>
        <cfreturn qGet>
    </cffunction>

    <cffunction name="getRemoveQuery" access="private" output="false" returnType="query">
        <cfargument name="machineItemID" type="any" required="true">
        <cfset var qGet = "">
        <cfquery name="qGet" datasource="#session.datasource#">
             SELECT TOP 1000 [ID]
                  ,[MachineFK]
                  ,[JIN]
                  ,[SerialNumber] AS machineSerialNumber
                  ,[ExistingGameName] AS machineGameName
                  ,[WarehouseFK]
                  ,[ClientAccounts].ACCNO AS WarehouseACCNO
                  ,[ManufacturerFK]
                  ,[GANFK]
                  ,[ProposedGameFK]
                  ,[ActionFK]
                  ,[Cost]
                  ,[RecordCreatedon]
                  ,[RecordCreatedBy]
                  ,[woSub_PO_number]
                  ,[IsActionCompleted]
                  ,[ActionCompletedBy]
                  ,[ActionCompletedOn]
              FROM [workOrderSub_MachineItem]
              INNER JOIN [ClientAccounts] on [ClientAccounts].AccountGUID =  [workOrderSub_MachineItem].WarehouseFK
              Where ID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.machineItemID#">
        </cfquery>
        <cfreturn qGet>
    </cffunction>

    <cffunction name="getInstallQuery" access="private" output="false" returnType="query">
        <cfargument name="machineItemID" type="any" required="true">
        <cfset var qGet = "">
        <cfquery name="qGet" datasource="#session.datasource#">
             SELECT TOP 1000 [workOrderSub_MachineItem].[ID]
                  ,[MachineFK]
                  ,[JIN]
                  ,[WarehouseFK]
                  ,[ManufacturerFK]
                  ,[GANFK]
                  ,[ProposedGameFK]
                  ,[ActionFK]
                  ,[Cost]
                  ,[RecordCreatedon]
                  ,[RecordCreatedBy]
                  ,[workOrderSub_MachineItem].[woSub_PO_number]
                  ,[custom_Machines].machineSerialNumber
                   ,[custom_Machines].machineGameName
                   ,[IsActionCompleted]
                  ,[ActionCompletedBy]
                  ,[ActionCompletedOn]
				  ,[workOrder].accnoFK AS venueAccno
              FROM [workOrderSub_MachineItem]
			  INNER JOIN [workOrderSub_Machine] on [workOrderSub_Machine].ID = [workOrderSub_MachineItem].MachineFK
			  INNER JOIN [workOrder] on [workOrder].ID = [workOrderSub_Machine].WorkOrderFK
              LEFT OUTER JOIN [custom_Machines] on [custom_Machines].machineGUID = [workOrderSub_MachineItem].ProposedGameFK
               Where [workOrderSub_MachineItem].ID =<cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.machineItemID#">
        </cfquery>
        <cfreturn qGet>
    </cffunction>

    <cffunction name="updateActionStatus" access="private" output="false" returntype="void">
        <cfargument name="machineItemID" type="any" required="true">
        <cfset var qSet = "">
        <cfquery name="qSet" datasource="#session.datasource#">
                UPDATE [workOrderSub_MachineItem]
                   SET [IsActionCompleted] = <cfqueryparam cfsqltype="cf_sql_bit" value="1">
                  ,[ActionCompletedBy] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.userID#">
                  ,[ActionCompletedOn] = GETDATE()
              WHERE ID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.machineItemID#">
        </cfquery>

    </cffunction>

    <cffunction name="emailVenue" access="private" output="false" returntype="void">
        <cfargument name="workOrderID" type="any" required="true">
        <cfset var qGet = "">
        <cfquery name="qGet" datasource="#session.datasource#">
              SELECT TOP 1000 [ID]
                  ,[accnoFK]
                  ,[ClientAccounts].EMAIL
              FROM [workOrder] INNER JOIN [ClientAccounts] on [ClientAccounts].ACCNO = [workOrder].accnoFK

              Where [ID] =  <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.workOrderID#">
        </cfquery>
        <cfset var mailTo = "debug@layerx.co.nz">
        <cfif qGet.recordcount neq 0 and len(qGet.EMAIL) gt 5>
            <cfset mailTo = qGet.EMAIL>
        </cfif>
        <cfset oMail = new Utils()>
        <cfset oMail.mailTo(subject="QEC Sync Required", content="A new machine action has been just performed. Please do a QEC sync.", emailto = mailTo )>
    </cffunction>

    <cffunction name="updateWorkOrderJobStatus" access="remote" output="false" returntype="struct" returnformat="JSON">
        <cfargument name="workOrderJobID" type="string" required="true">
        <cfargument name="statusValue" type="any" required="true">

        <cfscript>
            returnVal = StructNew();
            returnVal["success"] = false;
            returnVal["message"] = "";
        </cfscript>

        <cftry>
            <cfquery name="qUpdateWorkOrderJobStatus" datasource="#session.datasource#">
                UPDATE smeJobs
                SET jobStatus = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.statusValue#">
                <cfif arguments.statusValue eq 5>,jobCompleteStamp = getDate(), jobLastUpdated	= getDate()</cfif>
                WHERE jobID = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#arguments.workOrderJobID#">
            </cfquery>

            <cfscript>
                returnVal["success"] = true;
                returnVal["message"] = "Work Order Job status has been updated.";

                return returnVal;
            </cfscript>

            <cfcatch type="any">
                <cfset returnVal["message"] = "Work Order Job status update has failed.">
                <cfreturn returnVal>
            </cfcatch>
        </cftry>
    </cffunction>

    <cffunction name="getUserJobsOtherThanWorkorderJobs" access="remote" returnType="struct" output="false" returnformat="JSON">        
        <cfset var qGet = ''>
        <cfquery name="qGet" datasource="#session.datasource#">
            SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
            SELECT top 1000 smeJobs.jobID, smeJobs.jobName, smeJobs.jobCreatedBy, smeJobs.jobStatus, 
								  smeJobs.jobType, smeJobs.jobDescription, smeJobs.jobCreationDate, smeJobs.jobLastUpdated, smeJobs.jobACCNO, smeJobs.jobSEQNO, smeJobs.jobNumber,
								  smeJobs.jobCompleteStamp,smeJobs.jobReference, ClientAccounts.NAME, smeJobs.isFromWorkOrder,lib_jobStatus.jobStatusName
				FROM smeJobs INNER JOIN ClientAccounts ON smeJobs.jobACCNO = ClientAccounts.ACCNO
                INNER JOIN lib_jobStatus ON smeJobs.jobStatus = lib_jobStatus.jobStatusID
				WHERE (
					    smeJobs.jobStatus NOT IN (
								 SELECT [jobStatusID]
									  FROM [lib_jobStatus]
										WHERE [jobStatusName] in (
										'complete'
										,'Closed'
										,'Cancelled'
									  )

						 )
				    )
				AND	jobDeleted = 0   
                AND smeJobs.jobID in (
                      	SELECT [JobFK]
                          FROM [lib_jobAssignedUsers]
                          INNER JOIN [assignmentUsers] on [assignmentUsers].auID = [lib_jobAssignedUsers].assignmentUserFK
                          INNER JOIN [lib_User_AssignmentUser] on [lib_User_AssignmentUser].[AUFK] = [lib_jobAssignedUsers].assignmentUserFK
                          WHERE [lib_User_AssignmentUser].[UserFK] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#session.userID#">
                ) 
                AND JobID NOT in (
                    SELECT JOBID FROM [workOrder_GreatPlain]
                    WHERE [WorkOrderID] in (
                        SELECT [ID]
                        FROM [workOrder]
                        WHERE [workOrder].ID in (
                                SELECT [WorkOrderFK]
                            FROM [workOrderSub_Machine]
                            WHERE ID in (
                                SELECT [MachineFK]
                                FROM [workOrderSub_MachineItem]
                            )
                        )
                    )
                )
                
                ORDER BY jobCreationDate DESC
        </cfquery>

        <cfset var returnVal = structNew()>
        <cfset returnVal["success"] = true>
        <cfset returnVal["message"] = ''>
        <!--- setting the workOrderJobs object because this object is used throughout the application --->
        <cfset returnVal["workOrderJobs"] = arrayNew(1)>

        <cfset var newJobArray = arrayNew(1)>
        <cfset var startedJobArray = arrayNew(1)>
        <cfset var completedJobArray = arrayNew(1)>

        <cfloop query="qGet">            
           
            <cfset var aJobRecord = structNew()>
            <cfset ajobrecord["jobID"] = qGet.jobID>
            <cfset ajobrecord["jobName"] = ReplaceNoCase(qGet.jobName, 'Work Order: ', '', 'all')>
            <cfset ajobrecord["jobNumber"] = qGet.jobNumber>           
            <cfset ajobrecord["completedAction"] = "1">            
            <cfset ajobrecord["totalAction"] = "1">
            <cfif qGet.jobStatus eq 1>
                <cfset ajobrecord["status"] = 'new'>
                <cfset ajobrecord["class"] = 'label-success'>
                <cfset arrayappend(newJobArray, ajobrecord)>
            <cfelseif qGet.jobStatus gt 1 and qGet.jobStatus lt 4>
                <cfset ajobrecord["status"] = 'started'>
                <cfset ajobrecord["class"] = 'label-warning'>
                <cfset arrayappend(startedJobArray, ajobrecord)>
            <cfelse>
                <cfset ajobrecord["status"] = qGet.jobStatusName>
                <cfset ajobrecord["class"] = 'label-info'>
                <cfset arrayappend(completedJobArray, ajobrecord)>
            </cfif>
        </cfloop>

        <cfset var utils = new cfc.Utils()>
        <cfset returnVal["workOrderJobs"] = utils.arrayAppendAll(returnVal["workOrderJobs"], startedJobArray)>
        <cfset returnVal["workOrderJobs"] = utils.arrayAppendAll(returnVal["workOrderJobs"], newJobArray)>
        <cfset returnVal["workOrderJobs"] = utils.arrayAppendAll(returnVal["workOrderJobs"], completedJobArray)>

        <cfreturn returnVal />
    </cffunction>

</cfcomponent>
