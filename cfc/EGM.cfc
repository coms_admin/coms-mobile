<cfcomponent extends="cfc.Global">
	<cffunction name="updateMachineJin" access="public" output="false" returnType="boolean">
		<cfargument name="machineSerialNumber" type="string" required="true">
        <cfargument name="newJin" type="numeric" required="true">
		<cfargument name="machineAction" type="string" required="false" default="">

		<cfset var isSuccess = false>
		<cftry>
			<cfset logMe('Update', 'Update Machine [#arguments.machineSerialNumber#] JIN')>
			<cfset var qSet = "">
			<cfquery name="qSet" datasource="#session.datasource#">
					UPDATE [custom_Machines]
					   SET [machineJINNumber] = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.newJin#">
						, [lastUpdated] = GETDATE()
					WHERE machineSerialNumber = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.machineSerialNumber#">
			</cfquery>
			<cfset logMe('Insert', 'Insert a new Machine [#arguments.machineSerialNumber#] history')>
			<cfset insertMachineHistory(arguments.machineSerialNumber,arguments.machineAction)>
			<cfset isSuccess = true>
		<cfcatch type="any">
			<cfmail from="debug@layerx.co.nz" to="debug@layerx.co.nz" subject="Exception[COMS Mobile] - updateMachineAccno" type="HTML">
				<cfdump var="#cfcatch#">
			</cfmail>
		</cfcatch>
		</cftry>
		<cfreturn isSuccess>
	</cffunction>
	<cffunction name="getOtherActiveMachineHistoryByJIN" access="public" returntype="query" output="no">
		<cfargument name="accno" type="numeric" required="yes">
		<cfargument name="machineJINNumber" type="numeric" required="yes">
		<cfargument name="machineSN" type="string" required="yes">
		
		<cfset var qHistory = "">
		<cfquery name="qHistory" datasource="#application.datasource_cms#">
			SELECT [machineHistoryID]
			      ,[machineGUID]
			      ,[machineID]
			      ,[accno]
			      ,[machineName]
			      ,[machineDescription]
			      ,[machineMonitorType]
			      ,[machineModel]
			      ,[machineJINNumber]
			      ,[machineGameName]
			      ,[machineSerialNumber]
			      ,[recordCreated]
			      ,[lastUpdated]
			      ,[machineWarranty]
			      ,[machineHistoryCreated]
			      ,[StartDate]
			      ,[EndDate]
			      ,[lib_machineStatus].machineStatusName
			      ,[lib_machineStatus].machineStatusID
			  FROM [MachinesHistory]
			  LEFT OUTER JOIN [lib_machineStatus] on [lib_machineStatus].machineStatusID = [MachinesHistory].machineStatusFK
			  WHERE [accno] = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.accno#" />
			  AND machineJINNumber = <cfqueryparam cfsqltype="cf_sql_integer" value="#arguments.machineJINNumber#" />
			  AND machineSerialNumber <> <cfqueryparam cfsqltype="cf_sql_varchar" maxlength="255" value="#arguments.machineSN#" />
			  AND (EndDate IS NULL OR EndDate = '')
			  order by StartDate desc
		</cfquery>
		
		<cfreturn qHistory> 
	</cffunction>
	
	<cffunction name="updateMachineAccno" access="public" output="false" returnType="boolean">
		<cfargument name="machineSerialNumber" type="string" required="true">
		<cfargument name="newAccno" type="numeric" required="true">
		<cfargument name="insertHistory" type="boolean" required="no" default="true">

		<cfset var isSuccess = false>
		<cftry>
			<cfset logMe('Update', 'Update Machine [#arguments.machineSerialNumber#] ACCNO.')>
			<cfset var qSet = "">
			<cfquery name="qSet" datasource="#session.datasource#">
					UPDATE [custom_Machines]
					   SET [accno] = <cfqueryparam cfsqltype="cf_sql_numeric" value="#arguments.newAccno#">
						, [lastUpdated] = GETDATE()
					WHERE machineSerialNumber = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.machineSerialNumber#">
			</cfquery>

			<cfif isdefined("arguments.insertHistory") and arguments.insertHistory>
				<cfset logMe('Insert', 'Insert a new Machine [#arguments.machineSerialNumber#] history')>
				<cfset insertMachineHistory(arguments.machineSerialNumber)>
			</cfif>
			<cfset isSuccess = true>
		<cfcatch type="any">
			<cfmail from="debug@layerx.co.nz" to="debug@layerx.co.nz" subject="Exception[COMS Mobile] - updateMachineAccno" type="HTML">
				<cfdump var="#cfcatch#">
			</cfmail>
		</cfcatch>
		</cftry>
		<cfreturn isSuccess>
	</cffunction>

	<cffunction name="updateMachineGameName" access="public" output="false" returnType="boolean">
		<cfargument name="machineSerialNumber" type="string" required="true">
		<cfargument name="newGameName" type="string" required="true">

		<cfset var isSuccess = false>
		<cftry>
			<cfset logMe('Update', 'Update Machine [#arguments.machineSerialNumber#] Game Name')>
			<cfset var qSet = "">
			<cfquery name="qSet" datasource="#session.datasource#">
                UPDATE [custom_Machines]
                   SET [machineGameName] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.newGameName#">
                , [lastUpdated] = GETDATE()
                WHERE machineSerialNumber = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.machineSerialNumber#">
			</cfquery>
			<cfset logMe('Insert', 'Insert a new Machine [#arguments.machineSerialNumber#] history')>
			<cfset insertMachineHistory(arguments.machineSerialNumber)>
			<cfset var isSuccess = true>
		<cfcatch type="any">
			<cfmail from="debug@layerx.co.nz" to="debug@layerx.co.nz" subject="Exception[COMS Mobile] - UpdateMachineGameName" type="HTML">
				<cfdump var="#cfcatch#">
			</cfmail>
		</cfcatch>
		</cftry>
		<cfreturn isSuccess>
	</cffunction>

	<cffunction name="insertMachineHistory" access="public" output="false" returnType="boolean">
		<cfargument name="machineSerialNumber" type="string" required="true">
		<cfargument name="machineAction" type="string" required="false" default="">

		<cftry>
			<cftransaction>

				<!---update the current history end date--->
				<cfset var yesterdayDate = dateAdd('d', -1, now())>
				<cfquery name="qUpdateMachineHistory" datasource="#session.datasource#">
					UPDATE [MachinesHistory]
						SET EndDate = <cfqueryparam cfsqltype="cf_sql_timestamp" value="#yesterdayDate#">
						WHERE [machineHistoryID] in (
							SELECT TOP 1 [machineHistoryID]
							  FROM [MachinesHistory]
							  WHERE machineSerialNumber = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.machineSerialNumber#">
							  ORDER BY StartDate desc
						  )
							AND EndDate is NULL
				</cfquery>


				<cfset qGetNewMachine = "">
				<cfquery name="qGetNewMachine" datasource="#session.datasource#">
							SELECT  [machineID]
								  ,[accno]
								  ,[machineName]
								  ,[machineDescription]
								  ,[machineMonitorType]
								  ,[machineModel]
								  ,[machineJINNumber]
								  ,[machineGameName]
								  ,[machineSerialNumber]
								  ,[recordCreated]
								  ,[lastUpdated]
								  ,[machineWarranty]
								  ,[machineGUID]
								  ,[machineStatusFK]
								  ,[dateMachineStatusUpdate]
								  ,[machineModelFK]
								  ,[machineGameFK]
								  ,[machineBaseFK]
								  ,[machineDenominationFK]
								  ,GameWarrantyExpiry
								  ,CabinetWarrantyExpiry
								  ,[GameApprovalNumber]
								  ,[Manufacturer]
								  ,[CAN]
								  ,[RTP]
								  ,[GameCreditValue]
								  ,[BAN]
							FROM [custom_Machines]
							WHERE [machineSerialNumber] = <cfqueryparam cfsqltype="cf_sql_varchar" value="#arguments.machineSerialNumber#">
				</cfquery>
				
				<cfif len(arguments.machineAction) gt 0>
					<!--- Deactivate other active machine history existing at this Jin position --->
					<cfset qOtherMachineHistory = getOtherActiveMachineHistoryByJIN(qGetNewMachine.accno,qGetNewMachine.machineJINNumber,qGetNewMachine.machineSerialNumber)>
					<cfif qOtherMachineHistory.recordCount gt 0>
						<cfif len(qOtherMachineHistory.EndDate[1]) is 0>
							<cfquery name="qUpdateOtherMachineHistory" datasource="#application.datasource#">
								UPDATE [MachinesHistory]
									SET [EndDate] = <cfqueryparam cfsqltype="cf_sql_date" value="#yesterdayDate#" />
								WHERE machineHistoryID = <cfqueryparam cfsqltype="cf_sql_varchar" value="#qOtherMachineHistory.machineHistoryID[1]#" />
							</cfquery>
						</cfif>
					</cfif>	
				</cfif>
				<!--- Insert a new machine hisotry --->
				<cfquery name="qNewMachineHistory" datasource="#session.datasource#">
					INSERT INTO [MachinesHistory]
						   (
						   machineGUID
						   ,[machineID]
						   ,[accno]
						   ,[machineName]
						   ,[machineDescription]
						   ,[machineMonitorType]

						   ,[machineModel]
						   ,[machineJINNumber]
						   ,[machineGameName]
						   ,[machineSerialNumber]
						   ,[recordCreated]

						   ,[lastUpdated]
						   ,[machineWarranty]
						   ,[machineHistoryCreated]
						   ,[StartDate]
							,[machineStatusFK]
							,[machineGameFK]
							,[machineModelFK]
							,[machineBaseFK]
							,[machineDenominationFK]
							,GameWarrantyExpiry
							,CabinetWarrantyExpiry
							,[GameApprovalNumber]
							,[Manufacturer]
							,[CAN]
							,[RTP]
							,[GameCreditValue]
							,[BAN]
					)
					VALUES
					(
							<cfqueryparam cfsqltype="cf_sql_varchar" value="#qGetNewMachine.machineGUID#" />

							,<cfqueryparam cfsqltype="cf_sql_integer" value="#qGetNewMachine.machineID#" />
							,<cfqueryparam cfsqltype="cf_sql_integer" value="#qGetNewMachine.ACCNO#" />
							,<cfqueryparam cfsqltype="cf_sql_varchar" value="#qGetNewMachine.MACHINENAME#" />
							,<cfqueryparam cfsqltype="cf_sql_varchar" value="#qGetNewMachine.MACHINEDESCRIPTION#" />
							,<cfqueryparam cfsqltype="cf_sql_varchar" value="#qGetNewMachine.MACHINEMONITORTYPE#" />


							<cfif isDefined("qGetNewMachine.MACHINEMODEL") AND len(qGetNewMachine.MACHINEMODEL) gt 0>
								,<cfqueryparam cfsqltype="cf_sql_varchar" value="#qGetNewMachine.MACHINEMODEL#" />
							<cfelse>
								,null
							</cfif>
							<cfif isNumeric(qGetNewMachine.MACHINEJINNUMBER)>
                                ,<cfqueryparam cfsqltype="cf_sql_integer" value="#qGetNewMachine.MACHINEJINNUMBER#" />
							<cfelse>
                                ,<cfqueryparam cfsqltype="cf_sql_integer" value="0" />
							</cfif>
							,<cfqueryparam cfsqltype="cf_sql_varchar" value="#qGetNewMachine.MACHINEGAMENAME#" />
							,<cfqueryparam cfsqltype="cf_sql_varchar" value="#qGetNewMachine.MACHINESERIALNUMBER#" />
							,<cfqueryparam cfsqltype="cf_sql_date" value="#qGetNewMachine.recordCreated#" />

							,getdate()
							,<cfqueryparam cfsqltype="cf_sql_varchar" value="#qGetNewMachine.MACHINEWARRANTY#" />
							,getdate()
							,getdate()
							<cfif len(qGetNewMachine.machineStatusFK) gt 0>
								,<cfqueryparam cfsqltype="cf_sql_integer" value="#qGetNewMachine.machineStatusFK#" />
							<cfelse>
								,null
							</cfif>

							<cfif len(qGetNewMachine.machineGameFK) is 36>
								,<cfqueryparam cfsqltype="cf_sql_varchar" value="#qGetNewMachine.machineGameFK#" />
							<cfelse>
								,null
							</cfif>

							<cfif len(qGetNewMachine.machineModelFK) is 36>
								,<cfqueryparam cfsqltype="cf_sql_varchar" value="#qGetNewMachine.machineModelFK#" />
							<cfelse>
								,null
							</cfif>

							<cfif len(qGetNewMachine.machineBaseFK) is 36>
								,<cfqueryparam cfsqltype="cf_sql_varchar" value="#qGetNewMachine.machineBaseFK#" />
							<cfelse>
								,null
							</cfif>

							<cfif len(qGetNewMachine.machineDenominationFK) is 36>
								,<cfqueryparam cfsqltype="cf_sql_varchar" value="#qGetNewMachine.machineDenominationFK#" />
							<cfelse>
								,null
							</cfif>

							<cfif len(qGetNewMachine.GameWarrantyExpiry) gt 0>
								,<cfqueryparam cfsqltype="cf_sql_date" value="#qGetNewMachine.GameWarrantyExpiry#" />
							<cfelse>
								,null
							</cfif>

							<cfif len(qGetNewMachine.CabinetWarrantyExpiry) gt 0>
								,<cfqueryparam cfsqltype="cf_sql_date" value="#qGetNewMachine.CabinetWarrantyExpiry#" />
							<cfelse>
								,null
							</cfif>

							<cfif isDefined("qGetNewMachine.GameApprovalNumber") AND len(qGetNewMachine.GameApprovalNumber) gt 0>
								,<cfqueryparam cfsqltype="cf_sql_varchar" value="#QGETNEWMACHINE.GameApprovalNumber#" />
							<cfelse>
								,null
							</cfif>


							<cfif isDefined("qGetNewMachine.Manufacturer") AND len(qGetNewMachine.Manufacturer) gt 0>
								,<cfqueryparam cfsqltype="cf_sql_varchar" value="#QGETNEWMACHINE.Manufacturer#" />
							<cfelse>
								,null
							</cfif>

							<cfif isDefined("qGetNewMachine.CAN") AND len(qGetNewMachine.CAN) gt 0>
								,<cfqueryparam cfsqltype="cf_sql_varchar" value="#QGETNEWMACHINE.CAN#" />
							<cfelse>
								,null
							</cfif>

							<cfif isDefined("qGetNewMachine.RTP") AND len(qGetNewMachine.RTP) gt 0>
								,<cfqueryparam cfsqltype="cf_sql_varchar" value="#QGETNEWMACHINE.RTP#" />
							<cfelse>
								,null
							</cfif>

							<cfif isDefined("qGetNewMachine.GameCreditValue") AND len(qGetNewMachine.GameCreditValue) gt 0>
								,<cfqueryparam cfsqltype="cf_sql_varchar" value="#QGETNEWMACHINE.GameCreditValue#" />
							<cfelse>
								,null
							</cfif>

							<cfif isDefined("qGetNewMachine.BAN") AND len(qGetNewMachine.BAN) gt 0>
								,<cfqueryparam cfsqltype="cf_sql_varchar" value="#QGETNEWMACHINE.BAN#" />
							<cfelse>
								,null
							</cfif>
					)
				</cfquery>

			</cftransaction>
			<cfcatch type="any">
				<cfmail from="debug@layerx.co.nz" to="fanhua@layerx.co.nz" subject="FZ Debug" type="HTML">
					<cfdump var="#cfcatch#">
				</cfmail>
			</cfcatch>
		</cftry>
		<cfreturn true>
	</cffunction>
</cfcomponent>
