<cfcomponent>
    <cffunction name="GetDatasource" access="public" returnType="any" output="false">
        <cfargument name="username" type="string" required="true">

        <cfif cgi.SERVER_NAME contains "sandbox">
            <cfreturn 'COMS_PC_SB'>
        </cfif>

        <cfset var datasource = "">
        <cfset var tempArray = listToArray(arguments.username, '.')>
        <cfif arraylen(tempArray) gt 1>
            <!---should have at least two elements to make sense, i.e. debug@coms.net.nz.gi  --->
            <cfset var prefix = tempArray[arraylen(tempArray)]> <!---Get the last Element --->
            <cfswitch expression="#prefix#">
                <cfcase value="gi"><cfset datasource = "COMS_GI"></cfcase>
                <cfcase value="pc"><cfset datasource = "COMS_PC"></cfcase>
                <cfcase value="nzct"><cfset datasource = "COMS_NZCT"></cfcase>
                <cfdefaultcase></cfdefaultcase>
            </cfswitch>
        </cfif>

        <cfreturn datasource>
    </cffunction>

    <cffunction name="logMe" output="No" access="public" hint="Logs Usage" returntype="void">
        <cfargument name="activityType" type="string" required="yes">
        <cfargument name="activityDescription" type="string" required="yes">
        <cfif not isDefined("session.userID")>
           <cfreturn>
        </cfif>
        <cfquery name="data" datasource="#session.datasource#">
			INSERT INTO auditTrail
                (upn
                ,fullName
                ,moduleName
                ,activityType
                ,activityDescription
            )
			VALUES
			    ('#session.logonUPN#'
			    ,'#session.fullName#'
			    ,'Mobile'
			    ,'#arguments.activityType#'
			    ,'#arguments.activityDescription#'
			)
		</cfquery>

    </cffunction>
</cfcomponent>