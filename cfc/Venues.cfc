<cfcomponent extends="global">

    <cffunction name="getManagedVenues" access="remote" output="false" returntype="struct" returnformat="JSON" hint="Get the list of venues that the current user is an Area Manager of.">
        <cfset var returnVal = structNew()>
        <cfset returnVal["success"] = true>
        <cfset returnVal["message"] = ''>
        <cfset returnVal["venueList"] = arrayNew(1)>

        <cfset qVenues = QueryNew("")>
        <cfquery name="qVenues" datasource="#session.datasource#">
            SELECT accno
            FROM ClientAccounts
            WHERE ISACTIVE = 'Y'
            AND GMVID IS NOT NULL
            AND GMVID <> ''
            AND GMVID <> '00'
            AND GMVID > 0
            AND ManagerFK = <cfqueryparam cfsqltype="cf_sql_idstamp" value="#session.userID#">
        </cfquery>

        <cfif qVenues.recordcount gt 0>
            <cfset returnVal.success = true>
            <cfloop query="qVenues">
                <cfset arrayAppend(returnVal.venueList, qVenues.accno)>
            </cfloop>
        </cfif>

        <cfreturn returnVal>
    </cffunction>

</cfcomponent>